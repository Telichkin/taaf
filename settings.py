import os


SELENIUM_HUB_ADDRESS = "http://localhost:4444/wd/hub"
HOST = os.environ.get("TA_HOST", "localhost")
PORT = os.environ.get("TA_PORT", "3000")
FRONTEND_URI = f"http://{HOST}:{PORT}"
LOGIN = os.environ.get("TA_LOGIN")
PASSWORD = os.environ.get("TA_PASSWORD")
STANDARD_WAIT = 10
