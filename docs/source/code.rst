API Documentation
*****************

This documentation covers both :class:`User <myaTA.User>` actions and
:mod:`constants <myaTA.constants>` which you can use in your end-to-end tests


User
----
.. autoclass:: myaTA.User

.. autoclass:: myaTA.user.user.UserFacade


Constants
---------

.. automodule:: myaTA.constants
   :undoc-members:

