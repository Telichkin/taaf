.. myaTA documentation master file, created by
   sphinx-quickstart on Fri Jan 27 11:30:55 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

MyaTA: Tech Advisor Automation
==============================

Library provides high-level API for write End-To-End tests in problem-oriented manner.

Simple example::

   from myaTA import User
   from myaTA.constants import *


   User.open_browser(BrowserBrand.CHROME, BrowserType.STANDALONE)
   User.open_url(Url.LOGIN_PAGE)
   User.login('SomeLogin', 'Str0nG!PWd')
   User.navigate_to_page(SiteNavigation.LOG_PAGE)
   User.should_see_url(Url.LOG_PAGE)

.. toctree::
   :maxdepth: 2

   manifesto
   code


Indices and table
=================

* :ref:`genindex`
* :ref:`search`
