from random import shuffle

import pytest
from _pytest import junitxml

# TODO: This pytest hooks isn't obvious and need documentation.
# TODO: Create class for all this hooks


@pytest.hookimpl(tryfirst=True)
def pytest_configure(config):
    record_testreport = junitxml._NodeReporter.record_testreport
    junitxml._NodeReporter.record_testreport = record_testreport_fake(record_testreport)


def record_testreport_fake(record_testreport):
    def fake(self, testreport):
        old_nodeid = testreport.nodeid
        testreport.nodeid = testreport.new_nodeid
        record_testreport(self, testreport)
        testreport.nodeid = old_nodeid
    return fake


@pytest.mark.tryfirst
def pytest_runtest_makereport(item, call, __multicall__):
    report = __multicall__.execute()
    report.custom_test_name = item.custom_test_name
    setattr(item, f"rep_{report.when}", report)
    return report


def pytest_collection_modifyitems(session, config, items):
    new_items = [item for item in items if "test" in item.keywords._markers]
    shuffle(new_items)
    session.items = new_items
    print(f"of which {len(new_items)} tests")
    return session


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_protocol(item, nextitem):
    add_custom_test_name(item)
    change_test_name(item)
    yield


def add_custom_test_name(item):
    custom_test_name_exist = item.keywords._markers["test"].args
    if custom_test_name_exist:
        custom_test_name = get_custom_test_name(item)
    else:
        custom_test_name = item.nodeid.split("::")[-1]
    item.custom_test_name = custom_test_name


def get_custom_test_name(pytest_item):
    test_name = pytest_item.keywords._markers["test"].args[0]
    parametrize_info = pytest_item._genid
    if parametrize_info is not None:
        test_name = test_name.format(parametrize_info)
    return test_name


def change_test_name(pytest_item):
    allure_testcase = pytest.allure._allurelistener.test
    allure_testcase.name = pytest_item.custom_test_name
    custom_name_is_readable = (pytest_item.nodeid.split("::")[-1] != pytest_item.custom_test_name)
    if custom_name_is_readable:
        allure_testcase.title = pytest_item.custom_test_name


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_logreport(report):
    add_new_junit_test_name(report)
    yield


def add_new_junit_test_name(report):
    report.old_nodeid = report.nodeid
    if report.custom_test_name is not None:
        suite_name = report.nodeid.split("::")[-3].replace("Test", " ")
        new_nodeid = f"{suite_name}::{report.custom_test_name}"
        report.new_nodeid = new_nodeid
    else:
        report.new_nodeid = report.nodeid
