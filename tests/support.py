import pytest
import allure

from TAAF import User


feature = allure.feature
story = allure.story
test = pytest.mark.test
with_parameters = pytest.mark.parametrize


class TestSuite:
    @pytest.yield_fixture(scope="function", autouse=True)
    def __setup_and_teardown_test_handler(self, request):
        yield self.__safe_set_up(self.set_up, self.tear_down)
        if request.node.rep_setup.passed and request.node.rep_call.failed:
            self.on_failure()
        self.tear_down()

    @pytest.yield_fixture(scope="class", autouse=True)
    def __setup_and_teardown_suite_handler(self, request):
        yield self.__safe_set_up(self.set_up_suite, self.tear_down_suite)
        self.tear_down_suite()

    def __safe_set_up(self, set_up_function, tear_down_function):
        try:
            set_up_function()
        except Exception as e:
            self.on_failure()
            tear_down_function()
            raise e

    def set_up(self):
        pass

    def set_up_suite(self):
        pass

    def tear_down(self):
        User.close_browser()

    def tear_down_suite(self):
        pass

    def on_failure(self):
        User.attach_screenshot("Last screenshot")
        User.attach_logs()


def skip(parameters=None, message="Test skipped"):
    skip_parameters = pytest.mark.skipif(f"\"{message}\"", parameters)
    skip_parameters.__str__ = lambda: str(parameters)
    return skip_parameters


def with_procedures(*procedure_names):
    values = [story(procedure_name.__str__())(procedure_name) for procedure_name in procedure_names]
    return pytest.mark.parametrize("procedure_name", values)
