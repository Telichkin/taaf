from TAAF.test_library import User
from TAAF.test_library.constants import *
from .support import TestSuite, feature, story, test, with_parameters, skip


class CommonPagesTestSuite(TestSuite):
    def set_up(self):
        User.open_browser(BrowserBrand.CHROME, BrowserType.STANDALONE)
        User.open_url(Url.LOGIN_PAGE)
        User.login()
        User.should_see_url(Url.HARDWARE_PAGE)
        User.connect_to_cluster(DefaultConnectivitySettings.IP_ADDRESS)
        User.should_be_connected_to_cluster()


@feature("Navigation & Connection")
class PagesTestSuite(CommonPagesTestSuite):
    @test
    @with_parameters(
        ("page", "url"),
        [
            story(SiteNavigation.LOG)((SiteNavigation.LOG, Url.LOG_PAGE)),
            story(SiteNavigation.INVENTORY)((SiteNavigation.INVENTORY, Url.INVENTORY_PAGE + '/clusters')),
            story(SiteNavigation.HARDWARE)((SiteNavigation.HARDWARE, Url.HARDWARE_PAGE))
        ],
        ids=[SiteNavigation.LOG, SiteNavigation.INVENTORY, SiteNavigation.HARDWARE]
    )
    def right_page_should_be_opened(self, page, url):
        User.navigate_to_page(page)
        User.should_see_url(url)

    @test
    @story("Cluster connection")
    def can_connect_to_other_cluster(self):
        User.change_connectivity_settings({
            ConnectivitySettingsField.IP_ADDRESS: DefaultConnectivitySettings.IP_ADDRESS
        })
        User.should_be_connected_to_cluster()


@feature("Cluster Inventory")
@story(SiteNavigation.INVENTORY)
class InventoryPageTestSuite(CommonPagesTestSuite):
    def set_up(self):
        super().set_up()
        User.navigate_to_page(SiteNavigation.INVENTORY)

    @test
    def inventory_buttons_display_correct_number_of_elements(self):
        User.should_see_correct_number_of_elements_on_every_button()

    @test
    def inventory_elements_contains_correct_number_of_elements(self):
        User.should_see_correct_number_of_elements_inside_every_tab()

    @test
    @with_parameters("element_name", [ClusterElementName.CLUSTER])
    def inventory_element_in_table_contains_correct_number_of_nested_tables(self, element_name):
        User.should_see_correct_number_of_nested_tables(element_name)

    @test
    @with_parameters(("element_name", "nested_element_name"),
                     [(ClusterElementName.CLUSTER, ClusterElementName.BRICKS),
                      (ClusterElementName.CLUSTER, ClusterElementName.INFINIBAND_SWITCHES),
                      (ClusterElementName.CLUSTER, ClusterElementName.STORAGE_CONTROLLERS),
                      (ClusterElementName.CLUSTER, ClusterElementName.DAE),
                      (ClusterElementName.CLUSTER, ClusterElementName.SSD),
                      (ClusterElementName.CLUSTER, ClusterElementName.DPG),
                      (ClusterElementName.BRICKS, ClusterElementName.DAE),
                      (ClusterElementName.BRICKS, ClusterElementName.SSD),
                      (ClusterElementName.BRICKS, ClusterElementName.STORAGE_CONTROLLERS),
                      (ClusterElementName.STORAGE_CONTROLLERS, ClusterElementName.LOCAL_DISKS),
                      (ClusterElementName.STORAGE_CONTROLLERS, ClusterElementName.SC_PSU),
                      (ClusterElementName.DAE, ClusterElementName.DAE_PSU),
                      (ClusterElementName.DAE, ClusterElementName.DAE_CONTROLLERS),
                      (ClusterElementName.DAE, ClusterElementName.DAE_ROW_CONTROLLERS),
                      (ClusterElementName.DAE, ClusterElementName.DPG),
                      (ClusterElementName.DAE, ClusterElementName.SSD),
                      (ClusterElementName.DAE, ClusterElementName.DAE_FAN),
                      (ClusterElementName.DAE_CONTROLLERS, ClusterElementName.DAE_ROW_CONTROLLERS)])
    def nested_elements_contains_expected_number_of_rows_in_table(self, element_name, nested_element_name):
        User.should_see_expected_number_of_rows_in_table_of_nested_elements(element_name, nested_element_name)


@feature("Cluster Visualisation")
@story(SiteNavigation.HARDWARE)
class HardwarePageTestSuite(CommonPagesTestSuite):
    def set_up(self):
        super().set_up()
        User.navigate_to_page(SiteNavigation.HARDWARE)

    @test
    def cluster_visualisation_contains_correct_number_of_elements(self):
        User.should_see_correct_number_of_elements_in_visualisation()

    @test
    @with_parameters("brick_number", [1, 2])
    @with_parameters("brick_element_name", [
        ClusterElementName.LOCAL_DISKS,
        skip(ClusterElementName.SC_PSU, "After switch to other side of visualisation elements won't activate"),
        skip(ClusterElementName.DAE_PSU, "After switch to other side of visualisation elements won't activate"),
        skip(ClusterElementName.DAE_CONTROLLERS, "After switch to other side of visualisation elements won't activate"),
        skip(ClusterElementName.DAE_ROW_CONTROLLERS,
             "After switch to other side of visualisation elements won't activate"),
        skip(ClusterElementName.DPG, "After switch to other side of visualisation elements won't activate"),
        skip(ClusterElementName.SSD, "After switch to other side of visualisation elements won't activate"),
        skip(ClusterElementName.DAE_FAN, "After switch to other side of visualisation elements won't activate")
    ])
    def element_visualisation_has_tooltip(self, brick_number, brick_element_name):
        User.select_brick(brick_number)
        User.should_see_tooltip_on_every_brick_element(brick_element_name)
