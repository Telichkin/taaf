from .support import TestSuite, feature, test, with_procedures, skip
from TAAF import User
from TAAF.test_library.constants import *


@feature("Procedures")
class ProceduresTestSuite(TestSuite):
    def set_up(self):
        User.open_browser(BrowserBrand.CHROME, BrowserType.STANDALONE)
        User.open_url(Url.LOGIN_PAGE)
        User.login()
        User.should_see_url(Url.HARDWARE_PAGE)
        User.connect_to_cluster(DefaultConnectivitySettings.IP_ADDRESS)
        User.should_be_connected_to_cluster()

    @test("{} -- Abort")
    @with_procedures(
        ProcedureName.REPLACE_SSD,
        ProcedureName.REPLACE_STORAGE_CONTROLLER,
        ProcedureName.REPLACE_STORAGE_CONTROLLER_PSU,
        ProcedureName.REPLACE_DAE_CONTROLLER,
        ProcedureName.REPLACE_DAE_PSU,
        ProcedureName.REPLACE_DAE_FAN,
        ProcedureName.COLLECT_LOG_BUNDLE,
        ProcedureName.HW_BIST,
        ProcedureName.ID_LED_TEST,
        ProcedureName.SYSTEM_HEALTH_CHECK,
        ProcedureName.REPLACE_INFINIBAND_SWITCH,
        ProcedureName.REPLACE_DAE_ROW_CONTROLLER,
        ProcedureName.REPLACE_DAE,
        skip(ProcedureName.REPLACE_STORAGE_CONTROLLER_HDD, "Unexpected behavior. Can't click on element "
                                                           "with purpose 'Journal_and_boot_disk'. "
                                                           "Inconsistency of interface"),
        ProcedureName.CLUSTER_SCALE_UP
    )
    def procedure_can_be_aborted(self, procedure_name):
        User.open_procedure_page(procedure_name)
        User.abort_procedure()
        User.should_see_url(Url.HARDWARE_PAGE)
        User.should_be_connected_to_cluster()

    @test("{} -- End2End")
    @with_procedures(
        ProcedureName.REPLACE_SSD,
        ProcedureName.REPLACE_STORAGE_CONTROLLER,
        ProcedureName.REPLACE_STORAGE_CONTROLLER_PSU,
        ProcedureName.REPLACE_DAE_CONTROLLER,
        ProcedureName.REPLACE_DAE_PSU,
        ProcedureName.REPLACE_DAE_FAN,
        ProcedureName.REPLACE_DAE_ROW_CONTROLLER,
        skip(ProcedureName.REPLACE_INFINIBAND_SWITCH, "Test data for this procedure isn't implemented"),
        skip(ProcedureName.REPLACE_STORAGE_CONTROLLER_HDD, "Test data for this procedure isn't implemented"),
        skip(ProcedureName.REPLACE_DAE, "Test data for this procedure isn't implemented"),
        ProcedureName.COLLECT_LOG_BUNDLE,
        ProcedureName.HW_BIST,
        ProcedureName.ID_LED_TEST,
        ProcedureName.SYSTEM_HEALTH_CHECK,
        skip(ProcedureName.CLUSTER_SCALE_UP, "Test data for this procedure isn't implemented")
    )
    def procedure_can_be_successfully_finished(self, procedure_name):
        User.open_procedure_page(procedure_name)
        User.confirm_all_procedure_steps()
        User.should_see_in_procedure_report("Procedure completed successfully")
        User.finish_procedure()
        User.should_see_url(Url.HARDWARE_PAGE)
        User.should_be_connected_to_cluster()
