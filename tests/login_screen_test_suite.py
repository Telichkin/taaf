from .support import TestSuite, test, feature, story
from TAAF.test_library import User
from TAAF.test_library.constants import *


@feature("Login")
@story("Simple login")
class LoginTestSuite(TestSuite):
    def set_up(self):
        User.open_browser(BrowserBrand.CHROME, BrowserType.STANDALONE)
        User.open_url(Url.LOGIN_PAGE)

    @test
    def can_not_login_with_incorrect_credentials(self):
        User.login('InvalidLogin', 'InvalidPWD')
        User.should_not_logged_in()

    @test
    def can_login_with_correct_credentials(self):
        User.login()
        User.should_see_url(Url.HARDWARE_PAGE)
