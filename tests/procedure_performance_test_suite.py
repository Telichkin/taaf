from .support import TestSuite, test, with_procedures, feature, skip

from TAAF import User
from TAAF.test_library.constants import *


@feature("Procedure Performance")
@skip(message="Performance fix isn't in priority")
class ProcedurePerformanceTestSuite(TestSuite):
    def set_up(self):
        User.open_browser(BrowserBrand.CHROME, BrowserType.STANDALONE)
        User.open_url(Url.LOGIN_PAGE)
        User.login()
        User.should_see_url(Url.HARDWARE_PAGE)
        User.connect_to_cluster(DefaultConnectivitySettings.IP_ADDRESS)

    @test("{} -- End2End 10 times run")
    @with_procedures(*ProcedureName.constant_values())
    def performance_of_open_procedure_should_not_degradate(self, procedure_name):
        def scenario():
            User.open_procedure_page(procedure_name)
            User.wait_seconds(30)
            User.abort_procedure()
            User.should_see_url(Url.HARDWARE_PAGE)
            User.should_be_connected_to_cluster()

        User.should_not_see_performance_degradation(scenario, repeats=10, degradation_starts_from=9)

    @test("{} -- Component Selection")
    @with_procedures(*ProcedureName.constant_values())
    def performance_of_open_procedure_table_should_not_degradate(self, procedure_name):
        def scenario():
            User.open_procedure_inventory(procedure_name)
            User.close_procedure_inventory()
            User.should_see_url(Url.HARDWARE_PAGE)
            User.should_be_connected_to_cluster()

        User.should_not_see_performance_degradation(scenario, repeats=10, degradation_starts_from=2)
