from selenium import webdriver as _webdriver
from selenium.webdriver.remote.webdriver import WebDriver as _WebDriver

from TAAF.test_library.constants import BrowserBrand, BrowserType
from settings import SELENIUM_HUB_ADDRESS as _SELENIUM_HUB_ADDRESS, STANDARD_WAIT as _STANDARD_WAIT


class SeleniumDriverFabric:
    @staticmethod
    def get_driver(brand, browser_type) -> _WebDriver:
        if browser_type == BrowserType.STANDALONE:
            browser = SeleniumDriverFabric._get_standalone_browser(brand)
        elif browser_type == BrowserType.REMOTE:
            browser = SeleniumDriverFabric._get_remote_browser(brand)
        else:
            raise KeyError("No such browser type. You should use BrowserType from TAAF.constants")
        browser.set_window_size(1680, 920)
        return browser

    @staticmethod
    def _get_standalone_browser(brand) -> _WebDriver:
        drivers = {
            BrowserBrand.CHROME: _webdriver.Chrome,
            BrowserBrand.FIREFOX: _webdriver.Firefox,
            BrowserBrand.IE: _webdriver.Ie,
            BrowserBrand.SAFARI: _webdriver.Safari,
        }
        browser = drivers[brand]()
        return browser

    @staticmethod
    def _get_remote_browser(brand) -> _WebDriver:
        desired_capabilities = {
            BrowserBrand.CHROME: _webdriver.DesiredCapabilities.CHROME,
            BrowserBrand.FIREFOX: _webdriver.DesiredCapabilities.FIREFOX,
            BrowserBrand.IE: _webdriver.DesiredCapabilities.INTERNETEXPLORER,
            BrowserBrand.SAFARI: _webdriver.DesiredCapabilities.SAFARI,
        }
        browser = _webdriver.Remote(_SELENIUM_HUB_ADDRESS, desired_capabilities=desired_capabilities[brand])
        return browser
