import time

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException

from settings import STANDARD_WAIT
from TAAF.support import rerun_on_exception
from .selenium_driver_fabric import SeleniumDriverFabric
from .selenium_constants_converter import synchronize_locator


class SeleniumSingletonAdapter:
    __instance = None
    __driver: WebDriver = None

    def __new__(cls):
        if SeleniumSingletonAdapter.__instance is None:
            SeleniumSingletonAdapter.__instance = object.__new__(cls)
        return SeleniumSingletonAdapter.__instance

    def set_driver(self, driver_brand, driver_type):
        self.__driver = SeleniumDriverFabric.get_driver(driver_brand, driver_type)

    def close_driver(self):
        self.__driver.quit()
        self.__driver = None

    def open_url(self, url):
        self.__driver.get(url)

    def get_current_url(self):
        return self.__driver.current_url

    def is_page_loaded(self):
        return self.__driver.execute_script("return document.readyState;") == "complete"

    def get_screenshot(self):
        return self.__driver.get_screenshot_as_png()

    @synchronize_locator
    @rerun_on_exception(3, StaleElementReferenceException)
    def click_element_by_locator(self, locator, timeout=STANDARD_WAIT):
        self.find_element_by_locator(locator, timeout)
        element = WebDriverWait(self.__driver, timeout).until(
            expected_conditions.element_to_be_clickable(locator)
        )
        self.__wait_element_animation_stopped(element)
        element.click()

    @synchronize_locator
    def get_inner_text_by_locator(self, locator, timeout=STANDARD_WAIT):
        return self.find_element_by_locator(locator, timeout).text

    @synchronize_locator
    @rerun_on_exception(3, StaleElementReferenceException)
    def fill_input_by_locator(self, locator, text, timeout=STANDARD_WAIT):
        element = self.find_element_by_locator(locator, timeout)
        self.__wait_element_animation_stopped(element)
        element.send_keys(text)

    @synchronize_locator
    @rerun_on_exception(3, StaleElementReferenceException)
    def delete_text_in_input_by_locator(self, locator, timeout=STANDARD_WAIT):
        element = self.find_element_by_locator(locator, timeout)
        self.__wait_element_animation_stopped(element)
        element.clear()

    @synchronize_locator
    @rerun_on_exception(3, StaleElementReferenceException)
    def get_number_of_row_by_table_locator(self, locator, timeout=STANDARD_WAIT):
        table = self.find_element_by_locator(locator, timeout)
        table_body = table.find_element_by_xpath("tbody")
        return len(table_body.find_elements_by_xpath("tr"))

    @synchronize_locator
    def get_number_of_elements_in_list_by_locator(self, locator, timeout=STANDARD_WAIT):
        ul_list = self.find_element_by_locator(locator, timeout)
        return len(ul_list.find_elements_by_tag_name("li"))

    @synchronize_locator
    def find_element_by_locator(self, locator, timeout=STANDARD_WAIT):
        self.__wait_element_visibility_by_locator(locator, timeout)
        self.__driver.switch_to.active_element
        return self.__driver.find_element(*locator)

    @synchronize_locator
    def find_elements_by_locator(self, locator, timeout=STANDARD_WAIT):
        self.__wait_element_visibility_by_locator(locator, timeout)
        self.__driver.switch_to.active_element
        return self.__driver.find_elements(*locator)

    @synchronize_locator
    def __wait_element_visibility_by_locator(self, locator, timeout=STANDARD_WAIT):
        start_time = time.time()
        while timeout > 0:
            try:
                return WebDriverWait(self.__driver, timeout).until(
                    expected_conditions.visibility_of_element_located(locator)
                )
            except TimeoutException:
                raise TimeoutError(f"Can't find elements with locator: {locator}")
            except StaleElementReferenceException:
                time.sleep(0.1)
                iteration_duration = time.time() - start_time
                timeout -= iteration_duration

        raise TimeoutError(f"Can't find elements with locator: {locator}")

    @staticmethod
    @rerun_on_exception(3, StaleElementReferenceException)
    def __wait_element_animation_stopped(element):
        previous_element_location = element.location
        number_of_coincidence = 0
        for _ in range(50):
            time.sleep(0.1)
            current_element_location = element.location

            if current_element_location == previous_element_location:
                number_of_coincidence += 1
            else:
                number_of_coincidence = 0

            if number_of_coincidence == 2:
                return element
            previous_element_location = element.location

        raise Exception("Can't click on element, because animation didn't stop after 5 seconds")

    @synchronize_locator
    def wait_element_disappear_by_locator(self, locator, timeout=STANDARD_WAIT):
        try:
            WebDriverWait(self.__driver, timeout).until(
                expected_conditions.invisibility_of_element_located(locator)
            )
        except TimeoutException:
            raise TimeoutError(f"Element not disappeared after {timeout} seconds")
