from selenium.webdriver.common.by import By as SeleniumBy

from TAAF.test_library.constants import By


def synchronize_locator(fn):
    def wrapper(self, locator, *args, **kwargs):
        locator = __get_correct_locator(locator)
        return fn(self, locator, *args, **kwargs)
    return wrapper


def __get_correct_locator(locator):
    selenium_by_constants = {
        By.CLASS_NAME: SeleniumBy.CLASS_NAME,
        By.CSS_SELECTOR: SeleniumBy.CSS_SELECTOR,
        By.NAME: SeleniumBy.NAME,
        By.ID: SeleniumBy.ID,
        By.LINK_TEXT: SeleniumBy.LINK_TEXT,
        By.PARTIAL_LINK_TEXT: SeleniumBy.PARTIAL_LINK_TEXT,
        By.XPATH: SeleniumBy.XPATH,
        By.TAG_NAME: SeleniumBy.TAG_NAME
    }
    if locator[0] in selenium_by_constants.values():
        return locator
    else:
        selenium_by = selenium_by_constants[locator[0]]
        return selenium_by, locator[1]
