import allure
from allure.constants import AttachmentType

from TAAF.test_library.app_environment import Environment
from TAAF.test_library.constants import Service, LogType


class EnvironmentActions:
    @staticmethod
    def attach_services_logs():
        allure.attach("JythonServer.stdout.log",
                      Environment.get_log(service=Service.JYTHON, log_type=LogType.STDOUT),
                      AttachmentType.TEXT)
        allure.attach("JythonServer.stderr.log",
                      Environment.get_log(service=Service.JYTHON, log_type=LogType.STDERR),
                      AttachmentType.TEXT)
        allure.attach("Meteor.stdout.log",
                      Environment.get_log(service=Service.METEOR, log_type=LogType.STDOUT),
                      AttachmentType.TEXT),
        allure.attach("Meteor.stderr.log",
                      Environment.get_log(service=Service.METEOR, log_type=LogType.STDERR),
                      AttachmentType.TEXT)
        allure.attach("MongoDB.stdout.log",
                      Environment.get_log(service=Service.MONGODB, log_type=LogType.STDOUT),
                      AttachmentType.TEXT)
        allure.attach("MongoDB.stderr.log",
                      Environment.get_log(service=Service.MONGODB, log_type=LogType.STDERR))
