from time import time


class PerformanceActions:
    @staticmethod
    def should_not_see_performance_degradation(scenario, repeats, degradation_starts_from):
        if repeats < 2:
            raise AttributeError(f"'repeats' argument should be more than 1, but was {repeats}")

        first_execution_time = PerformanceActions._get_execution_time(scenario)
        for _ in range(repeats-2):
            scenario()
        last_execution_time = PerformanceActions._get_execution_time(scenario)

        degradation = last_execution_time - first_execution_time
        assert degradation < degradation_starts_from, \
            f"Time degradation should be less than {degradation_starts_from} seconds, but was {degradation} seconds"

    @staticmethod
    def _get_execution_time(function):
        start_time = time()
        function()
        return time() - start_time

    @staticmethod
    def should_perform_scenario_faster_than(scenario, seconds):
        execution_time = PerformanceActions._get_execution_time(scenario)
        assert execution_time <= seconds, \
            f"Execution time of scenario should be less that {seconds} seconds, but was {execution_time} seconds"
