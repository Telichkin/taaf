from .locators import *
from ..common import BasePage
from TAAF.test_library.constants import Url, LoginPageMessages


class LoginPage(BasePage):
    _URL = Url.LOGIN_PAGE

    def is_server_connectivity_exist(self):
        return self.get_login_message() != LoginPageMessages.NO_SERVER_CONNECTIVITY

    def get_login_message(self):
        return self._browser.get_inner_text_by_locator(LOGIN_MESSAGE)

    def enter_credentials(self, login, password):
        self._browser.fill_input_by_locator(LOGIN_FIELD, login)
        self._browser.fill_input_by_locator(PASSWORD_FIELD, password)

    def submit_credentials(self):
        self._browser.click_element_by_locator(LOGIN_BUTTON)
