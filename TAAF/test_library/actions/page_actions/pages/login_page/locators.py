from TAAF.test_library.constants import By as _By

LOGIN_FIELD = (_By.NAME, "user")
PASSWORD_FIELD = (_By.NAME, "pass")
LOGIN_BUTTON = (_By.CSS_SELECTOR, "#loginForm > div.text-center > button")
LOGIN_MESSAGE = (_By.CLASS_NAME, "login-message")
