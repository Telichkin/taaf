from ..common import CommonLoggedInPage
from TAAF.test_library.constants import Url


class AlertsPage(CommonLoggedInPage):
    _URL = Url.ALERTS_PAGE
