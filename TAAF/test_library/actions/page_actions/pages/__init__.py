from .alerts_page import AlertsPage
from .hardware_page import HardwarePage
from .inventory_page import InventoryPage
from .log_page import LogPage
from .login_page import LoginPage
from .procedure_page import ProcedurePage
from .common.logged_in_page import CommonLoggedInPage
from .common.base_page import BasePage
