from ..common import CommonLoggedInPage
from TAAF.test_library.constants import Url


class LogPage(CommonLoggedInPage):
    _URL = Url.LOG_PAGE
