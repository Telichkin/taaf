from ..common.logged_in_locators import *
from ..support import Locator as _Locator
from TAAF.test_library.constants import By as _By, ClusterElementName


class Rack(_Locator):
    SELF = (_By.CLASS_NAME, "rack")
    UNITS = (_By.CLASS_NAME, "unit")
    ACTIVE_BRICKS = (_By.CSS_SELECTOR, "div.unit.brick.fill")


class BrickElementNumber(_Locator):
    __map_with__ = ClusterElementName

    CLUSTER = None
    BRICKS = None
    STORAGE_CONTROLLERS = None
    DAE = None
    INFINIBAND_SWITCHES = None
    SSD = (_By.CSS_SELECTOR, "a.hw-inv-ssd:nth-of-type(1) > span")
    DAE_PSU = (_By.CSS_SELECTOR, "a.hw-inv-daepsu:nth-of-type(1) > span")
    DAE_CONTROLLERS = (_By.CSS_SELECTOR, "a.hw-inv-daelcc:nth-of-type(1) > span")
    DAE_ROW_CONTROLLERS = (_By.CSS_SELECTOR, "a.hw-inv-daeisim:nth-of-type(1) > span")
    SC_PSU = None
    SC_PSU_SC1 = (_By.CSS_SELECTOR, "a.hw-inv-scpsu-sc1:nth-of-type(1) > span")
    SC_PSU_SC2 = (_By.CSS_SELECTOR, "a.hw-inv-scpsu-sc2:nth-of-type(1) > span")
    LOCAL_DISKS = None
    LOCAL_DISKS_SC1 = (_By.CSS_SELECTOR, "a.hw-inv-localdisk-sc1:nth-of-type(1) > span")
    LOCAL_DISKS_SC2 = (_By.CSS_SELECTOR, "a.hw-inv-localdisk-sc2:nth-of-type(1) > span")
    DPG = (_By.CSS_SELECTOR, "a.hw-inv-dpg:nth-of-type(1) > span")
    DAE_FAN = (_By.CSS_SELECTOR, "a.hw-inv-daefan:nth-of-type(1) > span")


class ClusterElementButton(_Locator):
    __map_with__ = ClusterElementName

    CLUSTER = None
    BRICKS = None
    STORAGE_CONTROLLERS = None
    INFINIBAND_SWITCHES = None
    DAE = None
    SSD = (_By.CSS_SELECTOR, "a.hw-inv-ssd:nth-of-type(1)")
    DAE_PSU = (_By.CSS_SELECTOR, "a.hw-inv-daepsu:nth-of-type(1)")
    DAE_CONTROLLERS = (_By.CSS_SELECTOR, "a.hw-inv-daelcc:nth-of-type(1)")
    DAE_ROW_CONTROLLERS = (_By.CSS_SELECTOR, "a.hw-inv-daeisim:nth-of-type(1)")
    SC_PSU = None
    SC_PSU_SC1 = (_By.CSS_SELECTOR, "a.hw-inv-scpsu-sc1:nth-of-type(1)")
    SC_PSU_SC2 = (_By.CSS_SELECTOR, "a.hw-inv-scpsu-sc2:nth-of-type(1)")
    LOCAL_DISKS = None
    LOCAL_DISKS_SC1 = (_By.CSS_SELECTOR, "a.hw-inv-localdisk-sc1:nth-of-type(1)")
    LOCAL_DISKS_SC2 = (_By.CSS_SELECTOR, "a.hw-inv-localdisk-sc2:nth-of-type(1)")
    DPG = (_By.CSS_SELECTOR, "a.hw-inv-dpg:nth-of-type(1)")
    DAE_FAN = (_By.CSS_SELECTOR, "a.hw-inv-daefan:nth-of-type(1)")


class ClusterVisualisation(_Locator):
    ACTIVE_ELEMENT = (_By.CLASS_NAME, "highlighted")
    TOOLTIP = (_By.CSS_SELECTOR, "div.popover-data")
    CLOSE_TOOLTIP = (_By.CSS_SELECTOR, "div.fade > div.remove-tt")
