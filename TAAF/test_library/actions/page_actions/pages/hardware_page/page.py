from .locators import *
from ..common import CommonLoggedInPage
from TAAF.test_library.constants import Url, ClusterElementName, ElementNumberInOneCluster


class HardwarePage(CommonLoggedInPage):
    _URL = Url.HARDWARE_PAGE

    def get_number_of_bricks(self):
        return len(self._browser.find_elements_by_locator(Rack.ACTIVE_BRICKS))

    def select_brick(self, brick_number):
        all_bricks = self._browser.find_elements_by_locator(Rack.ACTIVE_BRICKS)
        all_bricks.reverse()
        brick_number_as_list_index = brick_number - 1
        all_bricks[brick_number_as_list_index].click()

    def get_number_of_elements(self, element_name):
        if element_name == ClusterElementName.DAE:
            number_of_elements = ElementNumberInOneCluster.DAE
        elif element_name == ClusterElementName.STORAGE_CONTROLLERS:
            number_of_elements = ElementNumberInOneCluster.STORAGE_CONTROLLERS
        elif element_name == ClusterElementName.CLUSTER:
            number_of_elements = ElementNumberInOneCluster.CLUSTER
        elif element_name == ClusterElementName.BRICKS:
            number_of_elements = ElementNumberInOneCluster.BRICKS
        elif element_name == ClusterElementName.INFINIBAND_SWITCHES:
            number_of_elements = ElementNumberInOneCluster.INFINIBAND_SWITCHES
        elif element_name == ClusterElementName.SC_PSU:
            sc_psu_in_sc1 = int(self._browser.get_inner_text_by_locator(BrickElementNumber.SC_PSU_SC1))
            sc_psu_in_sc2 = int(self._browser.get_inner_text_by_locator(BrickElementNumber.SC_PSU_SC2))
            number_of_elements = sc_psu_in_sc1 + sc_psu_in_sc2
        elif element_name == ClusterElementName.LOCAL_DISKS:
            local_disks_in_sc1 = int(self._browser.get_inner_text_by_locator(BrickElementNumber.LOCAL_DISKS_SC1))
            local_disks_in_sc2 = int(self._browser.get_inner_text_by_locator(BrickElementNumber.LOCAL_DISKS_SC2))
            number_of_elements = local_disks_in_sc1 + local_disks_in_sc2
        else:
            locator = BrickElementNumber.get_locator_for(element_name)
            number_of_elements = int(self._browser.get_inner_text_by_locator(locator))
        return number_of_elements

    def activate_elements(self, element_name):
        if element_name == ClusterElementName.SC_PSU:
            self._activate_button_by_locator(ClusterElementButton.SC_PSU_SC1)
            self._activate_button_by_locator(ClusterElementButton.SC_PSU_SC2)
        elif element_name == ClusterElementName.LOCAL_DISKS:
            self._activate_button_by_locator(ClusterElementButton.LOCAL_DISKS_SC1)
            self._activate_button_by_locator(ClusterElementButton.LOCAL_DISKS_SC2)
        else:
            self._activate_button_by_locator(ClusterElementButton.get_locator_for(element_name))

    def deactivate_elements(self, element_name):
        if element_name == ClusterElementName.SC_PSU:
            self._deactivate_button_by_locator(ClusterElementButton.SC_PSU_SC1)
            self._deactivate_button_by_locator(ClusterElementButton.SC_PSU_SC2)
        elif element_name == ClusterElementName.LOCAL_DISKS:
            self._deactivate_button_by_locator(ClusterElementButton.LOCAL_DISKS_SC1)
            self._deactivate_button_by_locator(ClusterElementButton.LOCAL_DISKS_SC2)
        else:
            self._deactivate_button_by_locator(ClusterElementButton.get_locator_for(element_name))

    def _activate_button_by_locator(self, element_button_locator):
        if not self._is_element_button_active(element_button_locator):
            self._browser.click_element_by_locator(element_button_locator)

    def _deactivate_button_by_locator(self, element_button_locator):
        if self._is_element_button_active(element_button_locator):
            self._browser.click_element_by_locator(element_button_locator)

    def _is_element_button_active(self, element_button_locator):
        return "active" in self._browser.find_element_by_locator(element_button_locator).get_attribute("class")

    def select_active_element(self, element_number):
        all_active_elements = self._browser.find_elements_by_locator(ClusterVisualisation.ACTIVE_ELEMENT)
        all_active_elements[element_number].click()

    def get_number_of_active_elements(self):
        return len(self._browser.find_elements_by_locator(ClusterVisualisation.ACTIVE_ELEMENT))

    def is_tooltip_opened(self):
        try:
            self._browser.find_element_by_locator(ClusterVisualisation.TOOLTIP)
            return True
        except TimeoutError:
            return False

    def close_opened_tooltip(self):
        self._browser.click_element_by_locator(ClusterVisualisation.CLOSE_TOOLTIP)
        self._browser.wait_element_disappear_by_locator(ClusterVisualisation.TOOLTIP)
