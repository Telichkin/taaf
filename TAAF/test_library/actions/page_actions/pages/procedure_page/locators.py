from ..common.base_locators import *
from ..support import Locator as _Locator
from TAAF.test_library.constants import By as _By


class Locator(_Locator):
    ABORT_BUTTON = (_By.CLASS_NAME, "btn-abort")
    ABORT_MESSAGE = (_By.CSS_SELECTOR, "#mainNavigation > ui-view > .ng-scope > div.wrapper > "
                                       "div.ng-scope > div.col-md-4.pull-right > span")
    FINISH_BUTTON = (_By.CLASS_NAME, "btn-done")
    INPUT_INFORMATION = (_By.CSS_SELECTOR, "xio-verifyaction > form > div > input")
    CONFIRM_BUTTON = (_By.CLASS_NAME, "btn-start")
    USER_ACTIONS_BOX = (_By.CLASS_NAME, "user-actions-box")


class PageInformation(_Locator):
    PSNT = (_By.CSS_SELECTOR, "div.rack-sn > div")
    ENTITY_ID = (_By.CSS_SELECTOR, "div.inventoryTableHead > div > div > xio-entity")
    USER_MESSAGE = (_By.CSS_SELECTOR, "td.user-action-message > div")
    REPORT_MESSAGE = (_By.CLASS_NAME, "report")
    POPOVER_DATA = (_By.CSS_SELECTOR, "div.popover-data > div > div.data > ul > li")
