from .locators import *
from ..common import BasePage
from TAAF.test_library.constants import *


class ProcedurePage(BasePage):
    _URL = Url.PROCEDURE_PAGE

    def press_abort_button(self):
        self._browser.click_element_by_locator(Locator.ABORT_BUTTON)

    def wait_end_of_abort_process(self, timeout):
        self._browser.wait_element_disappear_by_locator(Locator.ABORT_MESSAGE, timeout)

    def press_finish_button(self):
        self._browser.click_element_by_locator(Locator.FINISH_BUTTON)

    def is_procedure_finished(self):
        try:
            self._browser.find_element_by_locator(Locator.FINISH_BUTTON, timeout=1)
            return True
        except TimeoutError:
            return False

    def is_step_confirmation_available(self):
        try:
            self._browser.find_element_by_locator(Locator.CONFIRM_BUTTON, timeout=1)
            return True
        except TimeoutError:
            return False

    def get_information_type(self):
        user_message = self._browser.find_element_by_locator(PageInformation.USER_MESSAGE).text.lower()
        if ProcedurePageInformation.PSNT.lower() in user_message:
            return ProcedurePageInformation.PSNT
        elif ProcedurePageInformation.VERIFICATION_CODE.lower() in user_message:
            return ProcedurePageInformation.VERIFICATION_CODE
        elif ProcedurePageInformation.SERIAL_NUMBER.lower() in user_message:
            return ProcedurePageInformation.SERIAL_NUMBER
        elif ProcedurePageInformation.SERIAL_NUMBER_FULL.lower() in user_message:
            return ProcedurePageInformation.SERIAL_NUMBER_FULL
        else:
            return ProcedurePageInformation.UNKNOWN

    def confirm_step(self):
        self._browser.click_element_by_locator(Locator.CONFIRM_BUTTON)
        self._browser.wait_element_disappear_by_locator(Locator.CONFIRM_BUTTON)

    def fill_information(self, information):
        self._browser.fill_input_by_locator(Locator.INPUT_INFORMATION, information)

    def is_need_to_fill_information(self):
        try:
            self._browser.find_element_by_locator(Locator.INPUT_INFORMATION, timeout=3)
            return True
        except TimeoutError:
            return False

    def get_information_about(self, information_type):
        if information_type == ProcedurePageInformation.PSNT:
            return self._browser.find_element_by_locator(PageInformation.PSNT).text
        elif information_type == ProcedurePageInformation.SERIAL_NUMBER:
            return self.__get_element_serial_number()
        elif information_type == ProcedurePageInformation.SERIAL_NUMBER_FULL:
            # TODO: this S/N should be gotten from cluster visualisation inside procedure page.
            return "MQX60800428"
        elif information_type == ProcedurePageInformation.VERIFICATION_CODE:
            return "XtremIO"
        elif information_type == ProcedurePageInformation.UNKNOWN:
            return ProcedurePageInformation.UNKNOWN

    def __get_element_serial_number(self):
        self._browser.click_element_by_locator(PageInformation.ENTITY_ID)
        element_data_fields = self._browser.find_elements_by_locator(PageInformation.POPOVER_DATA)
        for data_field in element_data_fields:
            field_key = data_field.find_element_by_tag_name("span").text
            if "S/N" in field_key:
                return data_field.text.split(":", 1)[1].strip()

    def get_report_message(self):
        return self._browser.get_inner_text_by_locator(PageInformation.REPORT_MESSAGE)
