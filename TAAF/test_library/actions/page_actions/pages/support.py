from TAAF.support import ConstantMetaclass


class LocatorMetaclass(ConstantMetaclass):
    def __init__(cls, what, *args, **kwargs):
        super().__init__(what)
        if "__map_with__" in cls.__dict__:
            LocatorMetaclass.__check_map_correctness(cls)
            cls.__map__ = LocatorMetaclass.__create_map(cls)

    @staticmethod
    def __check_map_correctness(cls):
        locators_names = [name for name in cls.__dict__.keys()
                          if (not name.startswith("__") and name.isupper())]
        mapped_names = [name for name in cls.__map_with__.__dict__.keys()
                        if (not name.startswith("__") and name.isupper())]
        if not any([(mapped_name in locators_names) for mapped_name in mapped_names]):
            raise AttributeError("All names of class attributes should be the same as"
                                 " name of __map_with__ class attributes")

    @staticmethod
    def __create_map(cls):
        mapped_names = [name for name in cls.__map_with__.__dict__.keys()
                        if (not name.startswith("__") and name.isupper())]
        return {cls.__map_with__.__dict__[mapped_name]: cls.__dict__[mapped_name]
                for mapped_name in mapped_names}


class Locator(metaclass=LocatorMetaclass):
    @classmethod
    def get_locator_for(cls, constant_name):
        if "__map__" not in cls.__dict__:
            raise AttributeError("Locator not mapped to any ConstantClass")
        locator = cls.__dict__["__map__"][constant_name]
        if locator is None:
            raise AttributeError(f"Attribute {constant_name} doesn't have locator in {cls.__name__}")
        return locator
