from .base_locators import *
from ..support import Locator as _Locator
from TAAF.test_library.constants import By as _By, ProcedureMenuItem, ProcedureName, SiteNavigation, \
    ConnectivitySettingsField


class Hamburger(Hamburger):
    LOG_OUT_BUTTON = (_By.CSS_SELECTOR, "#xioTechAdvisor > header > div > table > tbody > tr > "
                                        "td.col-sm-1.hamburger-container.open-container > div > ul > "
                                        "li:nth-child(5)")


class Header(_Locator):
    CURRENT_CLUSTER_INDICATOR = (_By.CLASS_NAME, "app-global-indications")
    REFRESH_CLUSTER_BUTTON = (_By.CLASS_NAME, "refresh-cluster")
    CURRENT_CLUSTER_IP = (_By.CSS_SELECTOR, "#ta-state > div")


class TopSliderButtons(_Locator):
    __map_with__ = SiteNavigation

    SELF = (_By.CLASS_NAME, "inventory-slide")
    HARDWARE = (_By.CSS_SELECTOR, "xio-top-navigation > div > ul > li:nth-child(1)")
    INVENTORY = (_By.CSS_SELECTOR, "xio-top-navigation > div > ul > li:nth-child(2)")
    ALERTS = (_By.CSS_SELECTOR, "xio-top-navigation > div > ul > li:nth-child(3)")
    LOG = (_By.CSS_SELECTOR, "xio-top-navigation > div > ul > li:nth-child(4)")


class ProceduresSliderButtons(_Locator):
    __map_with__ = ProcedureMenuItem

    SELF = (_By.CSS_SELECTOR, "#rightWing")
    OPEN = (_By.CSS_SELECTOR, "#rightWing > div.handle")
    FRU = (_By.CSS_SELECTOR,
           "#rightWing > div.content > xio-procedure-spider > div > "
           "div.categories.categories-top > ul > li:nth-child(3)")
    CLUSTER_EXPANSION = (_By.CSS_SELECTOR,
                         "#rightWing > div.content > xio-procedure-spider > div > "
                         "div.categories.categories-top > ul > li:nth-child(2)")
    DIAGNOSTICS = (_By.CSS_SELECTOR,
                   "#rightWing > div.content > xio-procedure-spider > div > "
                   "div.categories.categories-top > ul > li:nth-child(1)")
    NDU = (_By.CSS_SELECTOR,
           "#rightWing > div.content > xio-procedure-spider > div > "
           "div.categories.categories-bottom > ul > li.category.curved.bl")
    ADMIN = (_By.CSS_SELECTOR,
             "#rightWing > div.content > xio-procedure-spider > div > "
             "div.categories.categories-bottom > ul > li.category.line")


class Procedures(_Locator):
    __map_with__ = ProcedureName

    REPLACE_DAE_CONTROLLER = (_By.CSS_SELECTOR,
                              "#rightWing > div.content > xio-procedure-spider > div > div.procedure-selector "
                              "> div > div.procedure.icon-daelcc")
    REPLACE_STORAGE_CONTROLLER_PSU = (_By.CSS_SELECTOR,
                                      "#rightWing > div.content > xio-procedure-spider > div > "
                                      "div.procedure-selector > div > div:nth-child(4)")
    REPLACE_STORAGE_CONTROLLER = (_By.CSS_SELECTOR,
                                  "#rightWing > div.content > xio-procedure-spider > div > "
                                  "div.procedure-selector > div > div.procedure.icon-sc")
    REPLACE_SSD = (_By.CSS_SELECTOR,
                   "#rightWing > div.content > xio-procedure-spider > div > div.procedure-selector > "
                   "div > div.procedure.icon-ssd")
    REPLACE_INFINIBAND_SWITCH = (_By.CSS_SELECTOR,
                                 "#rightWing > div.content > xio-procedure-spider > div > "
                                 "div.procedure-selector > div > div.procedure.icon-ibswitch")
    REPLACE_DAE_ROW_CONTROLLER = (_By.CSS_SELECTOR,
                                  "#rightWing > div.content > xio-procedure-spider > div > "
                                  "div.procedure-selector > div > div.procedure.icon-isim")
    REPLACE_DAE_FAN = (_By.CSS_SELECTOR,
                       "#rightWing > div.content > xio-procedure-spider > div > div.procedure-selector > "
                       "div > div.procedure.icon-fan")
    REPLACE_DAE_PSU = (_By.CSS_SELECTOR,
                       "#rightWing > div.content > xio-procedure-spider > div > div.procedure-selector > "
                       "div > div:nth-child(6)")
    REPLACE_DAE = (_By.CSS_SELECTOR,
                   "#rightWing > div.content > xio-procedure-spider > div > div.procedure-selector > "
                   "div > div.procedure.icon-dae")
    REPLACE_STORAGE_CONTROLLER_HDD = (_By.CSS_SELECTOR,
                                      "#rightWing > div.content > xio-procedure-spider > div > "
                                      "div.procedure-selector > div > div.procedure.icon-localdisk")
    CLUSTER_SCALE_UP = (_By.CSS_SELECTOR,
                        "#rightWing > div.content > xio-procedure-spider > div > div.procedure-selector > "
                        "div > div.procedure.icon-cluster")
    ID_LED_TEST = (_By.CSS_SELECTOR,
                   "#rightWing > div.content > xio-procedure-spider > div > div.procedure-selector > "
                   "div > div.procedure.icon-idled")
    SYSTEM_HEALTH_CHECK = (_By.CSS_SELECTOR,
                           "#rightWing > div.content > xio-procedure-spider > div > div.procedure-selector > "
                           "div > div:nth-child(3)")
    HW_BIST = (_By.CSS_SELECTOR,
               "#rightWing > div.content > xio-procedure-spider > div > div.procedure-selector "
               "> div > div:nth-child(2)")
    COLLECT_LOG_BUNDLE = (_By.CSS_SELECTOR,
                          "#rightWing > div.content > xio-procedure-spider > div > div.procedure-selector > "
                          "div > div.procedure.icon-log_bundle")
    INVENTORY_CLOSE_BUTTON = (_By.CSS_SELECTOR,
                              "#myModal > div > div > div.modal-header > button")
    INVENTORY_SEARCH_INPUT = (_By.CSS_SELECTOR,
                              "#myModal > div > div > div.modal-header > div > input")


class ConnectivitySettings(_Locator):
    __map_with__ = ConnectivitySettingsField

    DROPDOWN_BUTTON = (_By.ID, "connectivitySettingsBtn")
    IP_ADDRESS = (_By.NAME, "host")
    XMS_TECH_PASSWORD = (_By.NAME, "cliPassword")
    SSH_ROOT_PASSWORD = (_By.NAME, "ssh_password")
    IPMI_ADMIN_PASSWORD = (_By.NAME, "ipmi_password")
    SUBMIT_BUTTON = (_By.CSS_SELECTOR, "#connectivitySettingsForm > div > button")
    CANCEL_BUTTON = (_By.CSS_SELECTOR, "#connectivitySettings > button")
    LOADING_TEXT = (_By.CLASS_NAME, "loadingtext")


class ProceduresInventory(_Locator):
    SELF = (_By.ID, "myModal")
    FIRST_ELEMENT = (_By.CSS_SELECTOR, "#myModal > div > div > div.modal-body > "
                                       ".ng-scope > table > tbody > tr:nth-child(1)")
    CLOSE_BUTTON = (_By.CSS_SELECTOR,
                    "#myModal > div > div > div.modal-header > button")
    SEARCH_INPUT = (_By.CSS_SELECTOR,
                    "#myModal > div > div > div.modal-header > div > input")
