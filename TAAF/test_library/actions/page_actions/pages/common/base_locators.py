from ..support import Locator as _Locator
from TAAF.test_library.constants import By as _By


class Hamburger(_Locator):
    SELF = (_By.CLASS_NAME, "hamburger-container")
    HELP_BUTTON = (_By.CSS_SELECTOR, "#xioTechAdvisor > header > div > table > tbody > tr > "
                                     "td.col-sm-1.hamburger-container.open-container > div > ul > "
                                     "li:nth-child(2)")
    FULL_SCREEN_BUTTON = (_By.CSS_SELECTOR, "#xioTechAdvisor > header > div > table > tbody > tr > "
                                            "td.col-sm-1.hamburger-container.open-container > div > "
                                            "ul > li:nth-child(1)")
    ABOUT_BUTTON = (_By.CSS_SELECTOR, "#xioTechAdvisor > header > div > table > tbody > tr > "
                                      "td.col-sm-1.hamburger-container.open-container > div > "
                                      "ul > li:nth-child(3)")
