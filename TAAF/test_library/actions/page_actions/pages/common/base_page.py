from TAAF.support import Wait
from TAAF.test_library.browser import Browser
from settings import STANDARD_WAIT, FRONTEND_URI
from .base_locators import *


class BasePage:
    _URL: str = FRONTEND_URI

    def __init__(self):
        self._browser = Browser()
        Wait(STANDARD_WAIT, Exception("Incorrect URL was opened"))\
            .until(self._correct_url_to_be_opened)
        Wait(STANDARD_WAIT, TimeoutError("Page wasn't loaded"))\
            .until(self._browser.is_page_loaded)

    def _correct_url_to_be_opened(self):
        return self._URL in self._browser.get_current_url()

    def expand_full_screen(self):
        self._browser.click_element_by_locator(Hamburger.SELF)
        self._browser.click_element_by_locator(Hamburger.FULL_SCREEN_BUTTON)
