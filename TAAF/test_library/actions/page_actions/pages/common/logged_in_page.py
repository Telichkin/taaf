from TAAF.support import Wait
from .base_page import BasePage
from .logged_in_locators import *
from settings import STANDARD_WAIT
from TAAF.test_library.constants import *


class CommonLoggedInPage(BasePage):
    def __init__(self):
        super().__init__()

    def wait_connection_to_cluster(self):
        Wait(STANDARD_WAIT, TimeoutError(f"Connectivity to cluster is not established after {STANDARD_WAIT} seconds")) \
            .until(self._is_cluster_connectivity_exist)

    def _is_cluster_connectivity_exist(self):
        cluster_status_text = self._browser.get_inner_text_by_locator(Header.CURRENT_CLUSTER_INDICATOR)
        return (not cluster_status_text.isspace()) and (cluster_status_text != "")

    def _correct_url_to_be_opened(self):
        return self._URL in self._browser.get_current_url() and\
               self._browser.get_current_url() != Url.LOGIN_PAGE

    def logout(self):
        self._browser.click_element_by_locator(Hamburger.SELF)
        self._browser.click_element_by_locator(Hamburger.LOG_OUT_BUTTON)

    def get_current_cluster_ip(self):
        return self._browser.get_inner_text_by_locator(Header.CURRENT_CLUSTER_IP)

    def open_procedures_slider(self):
        self._browser.click_element_by_locator(ProceduresSliderButtons.OPEN)

    def is_procedure_slider_opened(self):
        procedure_slider = self._browser.find_element_by_locator(ProceduresSliderButtons.SELF)
        opened_width = procedure_slider.get_attribute("ps-size")
        return opened_width in procedure_slider.get_attribute("style")

    def select_procedures_menu_item(self, menu_item: str):
        self._browser.click_element_by_locator(ProceduresSliderButtons.get_locator_for(menu_item))

    def select_procedures_component(self, procedure_name: str):
        self._browser.click_element_by_locator(Procedures.get_locator_for(procedure_name))

    def select_first_element_in_procedure_inventory(self):
        self._browser.click_element_by_locator(ProceduresInventory.FIRST_ELEMENT)

    def close_procedure_inventory(self):
        self._browser.click_element_by_locator(ProceduresInventory.CLOSE_BUTTON)

    def open_top_slider(self):
        self._browser.click_element_by_locator(TopSliderButtons.SELF)

    def is_top_slider_opened(self):
        top_slider = self._browser.find_element_by_locator(TopSliderButtons.SELF)
        opened_height = top_slider.get_attribute("ps-size")
        return opened_height in top_slider.get_attribute("style")

    def select_navigation_to_page(self, page: str):
        self._browser.click_element_by_locator(TopSliderButtons.get_locator_for(page))

    def open_connectivity_settings(self):
        self._browser.click_element_by_locator(ConnectivitySettings.DROPDOWN_BUTTON)

    def close_connectivity_settings(self):
        self._browser.click_element_by_locator(ConnectivitySettings.CANCEL_BUTTON)

    def is_connectivity_settings_opened(self):
        return "open" in self._browser.find_element_by_locator(ConnectivitySettings.DROPDOWN_BUTTON)\
            .get_attribute("class")

    def submit_connectivity_settings(self):
        self._browser.click_element_by_locator(ConnectivitySettings.SUBMIT_BUTTON)
        self._browser.find_element_by_locator(ConnectivitySettings.LOADING_TEXT)
        self._browser.wait_element_disappear_by_locator(ConnectivitySettings.LOADING_TEXT, timeout=80)

    def change_connectivity_settings_field(self, field: str, value: str):
        self._browser.delete_text_in_input_by_locator(ConnectivitySettings.get_locator_for(field))
        self._browser.fill_input_by_locator(ConnectivitySettings.get_locator_for(field), value)
