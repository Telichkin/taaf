from .locators import *
from ..common import CommonLoggedInPage
from TAAF.test_library.constants import Url


class InventoryPage(CommonLoggedInPage):
    _URL = Url.INVENTORY_PAGE

    def get_number_of_bricks(self):
        number_of_bricks = self._browser.get_inner_text_by_locator(InventoryButtonsElementsNumber.BRICKS)
        number_of_bricks = number_of_bricks.split("\n")[0]
        return int(number_of_bricks)

    def get_number_of_elements_from_button(self, element_name):
        number_locator = InventoryButtonsElementsNumber.get_locator_for(element_name)
        number_of_elements = self._browser.get_inner_text_by_locator(number_locator)
        number_of_elements = number_of_elements.split("\n")[0]
        return int(number_of_elements)

    def get_number_of_elements_inside_tab(self, element_name):
        self.select_inventory_element(element_name)
        return self._browser.get_number_of_row_by_table_locator(InventoryElementTable.SELF)

    def select_inventory_element(self, element_name):
        element_tab_locator = InventoryButtons.get_locator_for(element_name)
        self._browser.click_element_by_locator(element_tab_locator)

    def get_number_of_nested_elements_from_tooltip(self):
        self._browser.click_element_by_locator(InventoryElementTable.FIRST_ELEMENT_INDICATOR)
        return self._browser.get_number_of_elements_in_list_by_locator(Tooltip.INVENTORY)

    def open_page_with_nested_elements(self, element_name):
        self.select_inventory_element(element_name)
        self._browser.click_element_by_locator(InventoryElementTable.FIRST_ELEMENT_INDICATOR)
        self._browser.click_element_by_locator(Tooltip.FIRST_INVENTORY_ELEMENT)

    def get_number_of_nested_tables(self):
        current_number_of_row = self._browser.get_number_of_row_by_table_locator(InventoryElementTable.SELF)
        self._browser.click_element_by_locator(InventoryElementTable.FIRST_ELEMENT_ARROW)
        new_number_of_row = self._browser.get_number_of_row_by_table_locator(InventoryElementTable.SELF)
        return new_number_of_row - current_number_of_row

    def get_number_of_nested_elements_from_button(self, nested_element_name):
        locator = NestedElementsPage.get_locator_for(nested_element_name)
        return int(self._browser.find_element_by_locator(locator).find_element_by_tag_name("span").text.split("\n")[0])

    def get_number_of_nested_elements_inside_tab(self, nested_element_name):
        self._browser.click_element_by_locator(NestedElementsPage.get_locator_for(nested_element_name))
        return self._browser.get_number_of_row_by_table_locator(NestedElementsPage.TABLE)

    def close_page_with_nested_elements(self):
        self._browser.click_element_by_locator(NestedElementsPage.BACK_BUTTON)
