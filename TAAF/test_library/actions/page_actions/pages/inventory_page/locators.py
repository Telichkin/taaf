from ..common.logged_in_locators import *
from TAAF.test_library.constants import By as _By, ClusterElementName
from ..support import Locator as _Locator


class InventoryButtons(_Locator):
    __map_with__ = ClusterElementName

    CLUSTER = (_By.ID, "inventory.clusters")
    BRICKS = (_By.ID, "inventory.bricks")
    STORAGE_CONTROLLERS = (_By.ID, "inventory.sc")
    INFINIBAND_SWITCHES = (_By.ID, "inventory.ibswitch")
    DAE = (_By.ID, "inventory.dae")
    SSD = (_By.ID, "inventory.ssd")
    DAE_PSU = (_By.ID, "inventory.daepsu")
    DAE_CONTROLLERS = (_By.ID, "inventory.daelcc")
    DAE_ROW_CONTROLLERS = (_By.ID, "inventory.daeisim")
    SC_PSU = (_By.ID, "inventory.scpsu")
    LOCAL_DISKS = (_By.ID, "inventory.localdisk")
    LINKS = (_By.ID, "inventory.link")
    DPG = (_By.ID, "inventory.dpg")
    DAE_FAN = None


class InventoryButtonsElementsNumber(_Locator):
    __map_with__ = ClusterElementName

    CLUSTER = (_By.XPATH, '//*[@id="inventory.clusters"]/span')
    BRICKS = (_By.XPATH, '//*[@id="inventory.bricks"]/span')
    STORAGE_CONTROLLERS = (_By.XPATH, '//*[@id="inventory.sc"]/span')
    INFINIBAND_SWITCHES = (_By.XPATH, '//*[@id="inventory.ibswitch"]/span')
    DAE = (_By.XPATH, '//*[@id="inventory.dae"]/span')
    SSD = (_By.XPATH, '//*[@id="inventory.ssd"]/span')
    DAE_PSU = (_By.XPATH, '//*[@id="inventory.daepsu"]/span')
    DAE_CONTROLLERS = (_By.XPATH, '//*[@id="inventory.daelcc"]/span')
    DAE_ROW_CONTROLLERS = (_By.XPATH, '//*[@id="inventory.daeisim"]/span')
    SC_PSU = (_By.XPATH, '//*[@id="inventory.scpsu"]/span')
    LOCAL_DISKS = (_By.XPATH, '//*[@id="inventory.localdisk"]/span')
    LINKS = (_By.XPATH, '//*[@id="inventory.link"]/span')
    DPG = (_By.XPATH, '//*[@id="inventory.dpg"]/span')
    DAE_FAN = None


class InventoryElementTable(_Locator):
    SELF = (_By.XPATH, '//*[@id="mainNavigation"]/ui-view/div/div[2]/div[2]/div/*/table')
    FIRST_ELEMENT_INDICATOR = (
        _By.XPATH,
        '//*[@id="mainNavigation"]/ui-view/div/div[2]/div[2]/div/*/table/tbody/tr/td[2]/xio-entity/div/div'
    )
    FIRST_ELEMENT_ARROW = (_By.XPATH, '//*[@id="inventory-view"]/*/table/tbody/tr/td[1]/span')


class Tooltip(_Locator):
    INVENTORY = (_By.CSS_SELECTOR, "div.popover-inventory-navigation > ul")
    FIRST_INVENTORY_ELEMENT = (_By.CSS_SELECTOR, "div.popover-inventory-navigation > ul > li:nth-child(1) > a")


class NestedElementsPage(_Locator):
    __map_with__ = ClusterElementName

    CLUSTER = None
    BRICKS = (_By.PARTIAL_LINK_TEXT, "X-Bricks")
    STORAGE_CONTROLLERS = (_By.PARTIAL_LINK_TEXT, "Storage Controllers")
    INFINIBAND_SWITCHES = (_By.PARTIAL_LINK_TEXT, "InfiniBand Switches")
    DAE = (_By.PARTIAL_LINK_TEXT, "DAEs")
    SSD = (_By.PARTIAL_LINK_TEXT, "SSDs")
    DAE_PSU = (_By.PARTIAL_LINK_TEXT, "DAE-PSUs")
    DAE_CONTROLLERS = (_By.PARTIAL_LINK_TEXT, "DAE Controllers")
    DAE_ROW_CONTROLLERS = (_By.PARTIAL_LINK_TEXT, "DAE Row Controllers")
    SC_PSU = (_By.PARTIAL_LINK_TEXT, "SC-PSUs")
    LOCAL_DISKS = (_By.PARTIAL_LINK_TEXT, "Local Disks")
    LINKS = None
    DPG = (_By.PARTIAL_LINK_TEXT, "DPGs")
    DAE_FAN = (_By.PARTIAL_LINK_TEXT, "DAE-FANs")
    TABLE = (_By.CLASS_NAME, "table")
    BACK_BUTTON = (_By.CSS_SELECTOR, ".pinnedBack > span")
