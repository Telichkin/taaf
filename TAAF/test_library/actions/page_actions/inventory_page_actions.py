from hamcrest import *

from TAAF.test_library.constants import *
from TAAF.test_library.support import contains_all_elements_from
from .pages import InventoryPage


class InventoryPageActions:
    @staticmethod
    def should_see_correct_number_of_elements_on_every_button():
        expected_number_of_elements = InventoryPageActions._get_expected_number_of_elements()
        number_of_elements_on_every_button = InventoryPageActions._get_number_of_elements_from_every_button()
        assert_that(expected_number_of_elements, contains_all_elements_from(number_of_elements_on_every_button))

    @staticmethod
    def should_see_correct_number_of_elements_inside_every_tab():
        expected_number_of_elements = InventoryPageActions._get_expected_number_of_elements()
        number_of_elements_inside_every_tab = InventoryPageActions._get_number_of_elements_inside_every_tab()
        assert_that(expected_number_of_elements, contains_all_elements_from(number_of_elements_inside_every_tab))

    @staticmethod
    def should_see_expected_number_of_rows_in_table_of_nested_elements(element_name, nested_element_name):
        inventory_page = InventoryPage()
        inventory_page.open_page_with_nested_elements(element_name)
        number_from_button = inventory_page.get_number_of_nested_elements_from_button(nested_element_name)
        number_from_tab = inventory_page.get_number_of_nested_elements_inside_tab(nested_element_name)
        inventory_page.close_page_with_nested_elements()
        assert number_from_tab == number_from_button, \
            f"Inside button displays {number_from_button} elements, but inside tab was {number_from_tab} elements"

    @staticmethod
    def _get_expected_number_of_elements():
        number_of_bricks = InventoryPage().get_number_of_bricks()
        one_brick_inventory = dict(OneBrickInventory.SELF)
        return {element_name: (element_count * number_of_bricks)
                for element_name, element_count in one_brick_inventory.items()}

    @staticmethod
    def _get_number_of_elements_from_every_button():
        inventory_page = InventoryPage()
        return {
            element_name: inventory_page.get_number_of_elements_from_button(element_name)
            for element_name in ClusterElementName.constant_values() if element_name not in
            [ClusterElementName.DAE_FAN]
            }

    @staticmethod
    def _get_number_of_elements_inside_every_tab():
        inventory_page = InventoryPage()
        return {
            element_name: inventory_page.get_number_of_elements_inside_tab(element_name)
            for element_name in ClusterElementName.constant_values() if element_name not in
            [ClusterElementName.DAE_FAN]
            }

    @staticmethod
    def should_see_correct_number_of_nested_tables(element_name):
        inventory_page = InventoryPage()
        inventory_page.select_inventory_element(element_name)
        expected_number_of_tables = inventory_page.get_number_of_nested_elements_from_tooltip()
        assert_that(inventory_page.get_number_of_nested_tables(), equal_to(expected_number_of_tables),
                    "Number of nested elements in tooltip and in the nested tables aren't the same")
