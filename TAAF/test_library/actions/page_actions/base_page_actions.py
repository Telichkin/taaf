from .pages import BasePage


class BasePageActions:
    @staticmethod
    def expand_full_screen():
        BasePage().expand_full_screen()
