import time

from hamcrest import *

from .pages import ProcedurePage
from .base_page_actions import BasePageActions


class ProcedurePageActions(BasePageActions):
    STEP_TIMEOUT = 1260

    @staticmethod
    def abort_procedure():
        page = ProcedurePage()
        page.press_abort_button()
        page.wait_end_of_abort_process(timeout=60)
        page.press_finish_button()

    @staticmethod
    def confirm_all_procedure_steps():
        page = ProcedurePage()
        while ProcedurePageActions.__is_next_step_available(page):
            if page.is_need_to_fill_information():
                ProcedurePageActions.__fill_information(page)
            page.confirm_step()

    @staticmethod
    def finish_procedure():
        page = ProcedurePage()
        page.press_finish_button()

    @staticmethod
    def should_see_in_procedure_report(message):
        page = ProcedurePage()
        assert_that(page.get_report_message(), contains_string(message))

    @staticmethod
    def __is_next_step_available(page):
        start_time = time.time()
        while not page.is_step_confirmation_available():
            if page.is_procedure_finished():
                return False
            if time.time() - start_time > ProcedurePageActions.STEP_TIMEOUT:
                raise TimeoutError(f"Next procedure step didn't open from {ProcedurePageActions.STEP_TIMEOUT} seconds")
            time.sleep(0.1)
        return True

    @staticmethod
    def __fill_information(page):
        information_type = page.get_information_type()
        information = page.get_information_about(information_type)
        page.fill_information(information)
