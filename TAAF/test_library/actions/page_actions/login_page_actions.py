from TAAF.support import Wait
from settings import STANDARD_WAIT
from .base_page_actions import BasePageActions
from .pages import LoginPage


class LoginPageActions(BasePageActions):
    @staticmethod
    def login(login, password):
        login_page = LoginPage()
        Wait(STANDARD_WAIT, TimeoutError("Connection to server isn't established"))\
            .until(login_page.is_server_connectivity_exist)
        login_page.enter_credentials(login, password)
        login_page.submit_credentials()

    @staticmethod
    def should_see_login_message(login_message):
        try:
            Wait(3).until(LoginPageActions._is_expected_login_message, (login_message, ))
        except TimeoutError:
            raise AssertionError(f"User expect to see '{login_message}' in login form\n"
                                 f"but was: '{LoginPage().get_login_message()}'")

    @staticmethod
    def _is_expected_login_message(login_message):
        return LoginPage().get_login_message() == login_message

    @staticmethod
    def should_not_logged_in():
        try:
            Wait(3).until(LoginPageActions._is_any_login_message_presented)
        except Exception:
            raise AssertionError(f"User should not be logged in\n"
                                 f"but was logged in")

    @staticmethod
    def _is_any_login_message_presented():
        return LoginPage().get_login_message().strip()
