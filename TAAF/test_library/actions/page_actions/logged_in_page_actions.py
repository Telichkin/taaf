from TAAF.test_library.constants import ProcedureMenuItem, ProcedureName, ConnectivitySettingsField

from .pages import CommonLoggedInPage


class CommonLoggedInPageActions:
    @staticmethod
    def open_procedure_page(procedure_name: str) -> None:
        logged_in_page = CommonLoggedInPage()
        CommonLoggedInPageActions.open_procedure_inventory(procedure_name)
        logged_in_page.select_first_element_in_procedure_inventory()

    @staticmethod
    def open_procedure_inventory(procedure_name: str) -> None:
        logged_in_page = CommonLoggedInPage()
        if not logged_in_page.is_procedure_slider_opened():
            logged_in_page.open_procedures_slider()
        menu_item = CommonLoggedInPageActions.__get_menu_item_for_procedure(procedure_name)
        logged_in_page.select_procedures_menu_item(menu_item)
        logged_in_page.select_procedures_component(procedure_name)

    @staticmethod
    def __get_menu_item_for_procedure(procedure_name: str) -> str:
        procedure_names_for_menu_item = {
            ProcedureMenuItem.FRU: [
                ProcedureName.REPLACE_DAE_CONTROLLER,
                ProcedureName.REPLACE_STORAGE_CONTROLLER_PSU,
                ProcedureName.REPLACE_STORAGE_CONTROLLER,
                ProcedureName.REPLACE_SSD,
                ProcedureName.REPLACE_INFINIBAND_SWITCH,
                ProcedureName.REPLACE_DAE_ROW_CONTROLLER,
                ProcedureName.REPLACE_DAE_FAN,
                ProcedureName.REPLACE_DAE_PSU,
                ProcedureName.REPLACE_DAE,
                ProcedureName.REPLACE_STORAGE_CONTROLLER_HDD
            ],
            ProcedureMenuItem.CLUSTER_EXPANSION: [
                ProcedureName.CLUSTER_SCALE_UP
            ],
            ProcedureMenuItem.DIAGNOSTICS: [
                ProcedureName.ID_LED_TEST,
                ProcedureName.SYSTEM_HEALTH_CHECK,
                ProcedureName.HW_BIST
            ],
            ProcedureMenuItem.NDU: [],
            ProcedureMenuItem.ADMIN: [
                ProcedureName.COLLECT_LOG_BUNDLE
            ],
        }

        for menu_item, procedure_names in procedure_names_for_menu_item.items():
            if procedure_name in procedure_names:
                return menu_item

        raise NoSuchProcedureException(
            f"Procedure with name {procedure_name} not found. You should use procedure names "
            f"from myaTA.constants module")

    @staticmethod
    def close_procedure_inventory() -> None:
        CommonLoggedInPage().close_procedure_inventory()

    @staticmethod
    def navigate_to_page(page: str) -> None:
        logged_in_page = CommonLoggedInPage()
        if not logged_in_page.is_top_slider_opened():
            logged_in_page.open_top_slider()
        logged_in_page.select_navigation_to_page(page)

    @staticmethod
    def connect_to_cluster(cluster_ip) -> None:
        logged_in_page = CommonLoggedInPage()
        wrong_cluster = (logged_in_page.get_current_cluster_ip() != cluster_ip)
        if wrong_cluster:
            CommonLoggedInPageActions.change_connectivity_settings(
                {ConnectivitySettingsField.IP_ADDRESS: cluster_ip}
            )

    @staticmethod
    def change_connectivity_settings(connectivity_settings: dict) -> None:
        logged_in_page = CommonLoggedInPage()
        if not logged_in_page.is_connectivity_settings_opened():
            logged_in_page.open_connectivity_settings()

        for field, value in connectivity_settings.items():
            logged_in_page.change_connectivity_settings_field(field, value)
        logged_in_page.submit_connectivity_settings()

    @staticmethod
    def should_be_connected_to_cluster() -> None:
        try:
            CommonLoggedInPage().wait_connection_to_cluster()
        except TimeoutError:
            raise AssertionError("We are not connected to the cluster")


class NoSuchProcedureException(Exception):
    pass
