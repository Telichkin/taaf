from hamcrest import *

from .pages import HardwarePage
from TAAF.test_library.constants import *
from TAAF.test_library.support import contains_all_elements_from


class HardwarePageActions:
    @staticmethod
    def should_see_correct_number_of_elements_in_visualisation():
        expected_number_of_elements = HardwarePageActions._get_expected_number_of_elements()
        number_of_elements_from_all_bricks = HardwarePageActions._get_number_of_elements_from_all_bricks()
        assert_that(expected_number_of_elements, contains_all_elements_from(number_of_elements_from_all_bricks))

    @staticmethod
    def _get_expected_number_of_elements():
        number_of_bricks = HardwarePage().get_number_of_bricks()
        one_brick_inventory = dict(OneBrickInventory.SELF)
        return {element_name: (element_count * number_of_bricks)
                for element_name, element_count in one_brick_inventory.items()}

    @staticmethod
    def _get_number_of_elements_from_all_bricks():
        hardware_page = HardwarePage()
        elements_from_all_bricks = {}
        for brick_number in range(1, hardware_page.get_number_of_bricks() + 1):
            elements_from_one_brick = HardwarePageActions()._get_number_of_elements_from_one_brick(brick_number)
            elements_from_all_bricks = {
                element_name: elements_from_all_bricks.get(element_name, 0) + element_value
                for element_name, element_value in elements_from_one_brick.items()
            }
        return elements_from_all_bricks

    @staticmethod
    def _get_number_of_elements_from_one_brick(brick_number):
        hardware_page = HardwarePage()
        hardware_page.select_brick(brick_number)
        return {element_name: hardware_page.get_number_of_elements(element_name)
                for element_name in ClusterElementName.constant_values()}

    @staticmethod
    def select_brick(brick_number):
        hardware_page = HardwarePage()
        hardware_page.select_brick(brick_number)

    @staticmethod
    def should_see_tooltip_on_every_brick_element(brick_element_name):
        hardware_page = HardwarePage()
        hardware_page.activate_elements(brick_element_name)
        number_of_elements_with_tooltip = len(
            [None for element_number in range(hardware_page.get_number_of_active_elements())
             if HardwarePageActions._is_active_element_has_tooltip(element_number)]
        )
        hardware_page.deactivate_elements(brick_element_name)
        real_number_of_elements = hardware_page.get_number_of_elements(brick_element_name)
        assert number_of_elements_with_tooltip == real_number_of_elements, \
            f"Expect that {real_number_of_elements} {brick_element_name} elements have tooltip, " \
            f"but only {number_of_elements_with_tooltip} elements have tooltip"

    @staticmethod
    def _is_active_element_has_tooltip(element_number):
        hardware_page = HardwarePage()
        hardware_page.select_active_element(element_number)
        if hardware_page.is_tooltip_opened():
            hardware_page.close_opened_tooltip()
            return True
        return False
