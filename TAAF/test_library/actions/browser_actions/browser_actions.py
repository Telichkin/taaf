import allure
from allure.constants import AttachmentType

from TAAF.support import Wait
from TAAF.test_library.browser import Browser


class BrowserActions:
    OPEN_PAGE_TIMEOUT = 15

    @staticmethod
    def open_browser(browser_brand, browser_type):
        Browser().set_driver(browser_brand, browser_type)

    @staticmethod
    def open_url(url: str):
        Browser().open_url(url)

    @staticmethod
    def close_browser():
        Browser().close_driver()

    @staticmethod
    def attach_screenshot(name):
        allure.attach(name, Browser().get_screenshot(), AttachmentType.PNG)

    @staticmethod
    def should_see_url(url: str):
        try:
            Wait(BrowserActions.OPEN_PAGE_TIMEOUT)\
                .until(BrowserActions._correct_url_to_be_opened, (url,))
        except TimeoutError:
            raise AssertionError(f"User should see url: '{url}'\n"
                                 f"but was: '{Browser().get_current_url()}'")

    @staticmethod
    def _correct_url_to_be_opened(url: str):
        return Browser().get_current_url() == url
