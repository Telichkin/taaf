import time


class OSActions:
    @staticmethod
    def wait_seconds(seconds):
        time.sleep(seconds)
