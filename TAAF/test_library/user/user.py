import allure

from TAAF.test_library.actions import *
from settings import LOGIN, PASSWORD


class UserFacade:
    @staticmethod
    @allure.step("Open browser")
    def open_browser(browser_brand: str, browser_type: str) -> None:
        """
        First action in every test, which opens specific browser

        :param browser_brand: :class:`BrowserBrand <myaTA.constants.BrowserBrand>` attribute
                from :mod:`constants <myaTA.constants>` module
        :param browser_type:  :class:`BrowserBrand <myaTA.constants.BrowserBrand>` attribute
                from :mod:`constants <myaTA.constants>` module
        """
        BrowserActions.open_browser(browser_brand, browser_type)

    @staticmethod
    @allure.step("Close browser")
    def close_browser() -> None:
        """
        Usually last action (in teardown method), which closes the open browser

        """
        BrowserActions.close_browser()

    @staticmethod
    @allure.step("Open url {0}")
    def open_url(url: str) -> None:
        """
        Open specific URL in current browser tab

        :param url: URL with protocol (e.g. http://facebook.com)
        """
        BrowserActions.open_url(url)

    @staticmethod
    @allure.step("Checking that current url is {0}")
    def should_see_url(url: str) -> None:
        """
        Checking that current browser tab has expected URL

        :param url: URL with protocol (e.g. http://facebook.com)
        """
        BrowserActions.should_see_url(url)

    @staticmethod
    @allure.step("Login")
    def login(login: str = LOGIN, password: str = PASSWORD) -> None:
        """
        Can perform only from login page

        """
        LoginPageActions.login(login, password)

    @staticmethod
    @allure.step("Checking that message in login form is '{0}'")
    def should_see_login_message(login_message):
        """
        Checking that message in login form is correct
        :param login_message: :class:`LoginPageMessages <myaTA.constants.LoginPageMessages>`
                attribute from :mod:`constants <myaTA.constants>` module
        :return:
        """
        LoginPageActions.should_see_login_message(login_message)

    @staticmethod
    @allure.step("Checking that user is not logged in")
    def should_not_logged_in() -> None:
        """
        Can perform only from login page
        Checking that user still staying on login page

        """
        LoginPageActions.should_not_logged_in()

    @staticmethod
    @allure.step("Expand full screen")
    def expand_full_screen() -> None:
        """
        Can perform from all pages

        """
        BasePageActions.expand_full_screen()

    @staticmethod
    @allure.step("Open procedure page for '{0}'")
    def open_procedure_page(procedure_name: str) -> None:
        """
        Can perform only from logged in pages (e.g. /hardware, /alerts, etc) except
        /procedures page

        :param procedure_name: :class:`ProcedureName <myaTA.constants.ProcedureName>`
                attribute from :mod:`constants <myaTA.constants>` module
        """
        CommonLoggedInPageActions.open_procedure_page(procedure_name)

    @staticmethod
    @allure.step("Open procedure inventory for '{0}'")
    def open_procedure_inventory(procedure_name: str) -> None:
        """
        Can perform only from logged in pages (e.g. /hardware, /alerts, etc) except
        /procedures pages

        :param procedure_name: :class:`ProcedureName <myaTA.constants.ProcedureName>`
                attribute from :mod:`constants <myaTA.constants>` module
        """
        CommonLoggedInPageActions.open_procedure_inventory(procedure_name)

    @staticmethod
    @allure.step("Close opened procedure inventory")
    def close_procedure_inventory() -> None:
        """
        Can perform when procedure inventory os opening

        """
        CommonLoggedInPageActions.close_procedure_inventory()

    @staticmethod
    @allure.step("Abort running procedure")
    def abort_procedure() -> None:
        """
        Can perform only from /procedure page

        """
        ProcedurePageActions.abort_procedure()

    @staticmethod
    @allure.step("Navigate to {0}")
    def navigate_to_page(page: str) -> None:
        """
        Can perform only from logged in pages (e.g. /hardware, /alerts, etc) except
        /procedures page

        :param page: :class:`SiteNavigation <myaTA.constants.SiteNavigation>` attribute from
                :mod:`constants <myaTA.constants>` module
        """
        CommonLoggedInPageActions.navigate_to_page(page)

    @staticmethod
    @allure.step("Checking connection to the cluster")
    def should_be_connected_to_cluster() -> None:
        """
        Can perform only from logged in pages (e.g. /hardware, /alerts, etc) except
        /procedures page

        """
        CommonLoggedInPageActions.should_be_connected_to_cluster()

    @staticmethod
    @allure.step("Change connectivity settings to {0}")
    def change_connectivity_settings(settings_to_change: dict) -> None:
        """
        Can perform only from logged in pages (e.g. /hardware, /alerts, etc) except
        /procedures page

        :param settings_to_change: dict where keys represent fields of connectivity settings form
                and should presents as :class:`ConnectivitySettingsField <myaTA.constants.ConnectivitySettingsField>`
                attributes from :mod:`constants <myaTA.constants>` module
        """
        CommonLoggedInPageActions.change_connectivity_settings(settings_to_change)

    @staticmethod
    @allure.step("Connect to cluster with ip '{0}'")
    def connect_to_cluster(cluster_ip: str) -> None:
        """
        Connect to appropriate cluster ip if we are not connected to this cluster right now.

        Can perform only from logged in pages (e.g. /hardware, /alerts, etc) except
        /procedures page

        """
        CommonLoggedInPageActions.connect_to_cluster(cluster_ip)

    @staticmethod
    def attach_screenshot(name: str) -> None:
        """
        Attach PNG screenshot to report

        :param name: name of screenshot inside report

        """
        BrowserActions.attach_screenshot(name)

    @staticmethod
    def attach_logs() -> None:
        """
        Attach logs from all services: Jython Server, Meteor, MongoDB

        """
        EnvironmentActions.attach_services_logs()

    @staticmethod
    @allure.step("Checking that after {repeats} runs time degradation is less than"
                 " {degradation_starts_from} seconds")
    def should_not_see_performance_degradation(scenario: callable,
                                               repeats: int, degradation_starts_from: int) -> None:
        """
        Runs some :param scenario :param repeats times and raises AssertionError when difference
        between last and first runs is more than :param degradation_starts_from

        :param scenario: callable object that describe some user scenario
        :param repeats: number of scenario repeats, should be more than 1
        :param degradation_starts_from: maximum allowable difference of time consumption between
                last and first runs of scenario
        """
        PerformanceActions.should_not_see_performance_degradation(scenario, repeats, degradation_starts_from)

    @staticmethod
    def should_perform_scenario_faster_than(scenario: callable, seconds: int) -> None:
        """
        Check that duration of :param scenario is less than :param seconds

        :param scenario: callable object that describe some user scenario
        :param seconds: maximum allowable duration of scenario
        """
        PerformanceActions.should_perform_scenario_faster_than(scenario, seconds)

    @staticmethod
    @allure.step("Checking that every buttons, that represent inventory elements, "
                 "contains appropriate number of this elements")
    def should_see_correct_number_of_elements_on_every_button() -> None:
        """
        Can perform only from /inventory page

        Number of elements depends only from number of bricks. Firstly we check number of bricks
        and after that check number of every every inventory element (inside button)

        """
        InventoryPageActions.should_see_correct_number_of_elements_on_every_button()

    @staticmethod
    @allure.step("Checking that when we select each inventory tab (PSU, SSD, etc) "
                 "on the page exist correct number of the elements")
    def should_see_correct_number_of_elements_inside_every_tab() -> None:
        """
        Can perform only from /inventory page

        Number of elements depends only from number of bricks. Firstly we check number of bricks
        and after that check number of every inventory element, clicking by every inventory tab
        and checking number of elements on the page

        """
        InventoryPageActions.should_see_correct_number_of_elements_inside_every_tab()

    @staticmethod
    @allure.step("Checking that all brick visualisations contain correct "
                 "number of elements")
    def should_see_correct_number_of_elements_in_visualisation() -> None:
        """
        Can perform only from /hardware page

        Number of elements depends only from number of bricks. Firstly we check number of bricks
        in the rack. After that we clicks by every brick in the rack and count number of inner
        elements such as SSD, PSU and so on

        """
        HardwarePageActions.should_see_correct_number_of_elements_in_visualisation()

    @staticmethod
    @allure.step("Confirm every step in opened procedure")
    def confirm_all_procedure_steps() -> None:
        """
        Can perform only from /procedures page

        Press "confirm" or "next stage" on every procedure step. If the step needs some
        input data, this method put needed data into input field

        """
        ProcedurePageActions.confirm_all_procedure_steps()

    @staticmethod
    @allure.step("Checking that '{0}' contains in procedure report message")
    def should_see_in_procedure_report(message: str) -> None:
        """
        Can perform only from /procedures page

        Check containing of the message in the procedure report

        """
        ProcedurePageActions.should_see_in_procedure_report(message)

    @staticmethod
    @allure.step("Finish ended procedure")
    def finish_procedure() -> None:
        """
        Can perform only from /procedures page

        Only finish already aborted or ended procedure

        """
        ProcedurePageActions.finish_procedure()

    @staticmethod
    @allure.step("Wait for {0} seconds")
    def wait_seconds(seconds) -> None:
        """
        Typical sleep

        """
        OSActions.wait_seconds(seconds)

    @staticmethod
    @allure.step("Select brick with number {0}")
    def select_brick(brick_number) -> None:
        """
        Can perform only from /hardware page

        Select brick in cluster visualisation by its number

        """
        HardwarePageActions.select_brick(brick_number)

    @staticmethod
    @allure.step("Checking tooltip on {0} element")
    def should_see_tooltip_on_every_brick_element(brick_element_name) -> None:
        """
        Can perform only from /hardware page

        Click on every brick element with :param brick_element_name and assert that
        after click tooltip will be opened

        """
        HardwarePageActions.should_see_tooltip_on_every_brick_element(brick_element_name)

    @staticmethod
    @allure.step("Checking number of nested tables")
    def should_see_correct_number_of_nested_tables(element_name) -> None:
        """
        Can perform only from /inventory page

        Checking that table visualisation in table contains correct number of nested tables

        """
        InventoryPageActions.should_see_correct_number_of_nested_tables(element_name)

    @staticmethod
    @allure.step("Open nested element ({1}) for {0} element")
    def should_see_expected_number_of_rows_in_table_of_nested_elements(element_name, nested_element_name) -> None:
        """
        Can perform only from /inventory page

        Open nested view of selected element and check that number of elements inside every button
        equal to number of rows in nested table

        """
        InventoryPageActions.should_see_expected_number_of_rows_in_table_of_nested_elements(element_name,
                                                                                            nested_element_name)
