from hamcrest.core.base_matcher import BaseMatcher


class IsDictionariesEquals(BaseMatcher):
    def __init__(self, actual_dictionary):
        self.actual_dictionary = actual_dictionary
        self.error_messages = []

    def _matches(self, expected_dictionary):
        for expected_key, expected_value in expected_dictionary.items():
            if not self._is_key_and_value_correct(expected_key, expected_value):
                error_message = self._generate_error_message(expected_key, expected_value)
                self.error_messages.append(error_message)
        return len(self.error_messages) == 0

    def _is_key_and_value_correct(self, expected_key, expected_value):
        try:
            actual_value = self.actual_dictionary[expected_key]
        except KeyError:
            return False
        return expected_value == actual_value

    def _generate_error_message(self, expected_key, expected_value):
        if expected_key not in self.actual_dictionary:
            return f"'{expected_key}' key is missing"
        elif self.actual_dictionary[expected_key] != expected_value:
            actual_value = self.actual_dictionary[expected_key]
            return f"'{expected_key}' key should be '{expected_value}', but was '{actual_value}'"

    def describe_to(self, description):
        description.append_text("equals dictionaries")
        return description

    def describe_mismatch(self, item, mismatch_description):
        full_error = ";\n          ".join(self.error_messages)
        mismatch_description.append_text(full_error)
        return mismatch_description


def contains_all_elements_from(actual_dictionary):
    return IsDictionariesEquals(actual_dictionary)
