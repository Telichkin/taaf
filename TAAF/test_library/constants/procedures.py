from TAAF.support import ConstantClass


class ProcedureMenuItem(ConstantClass):
    FRU = "FRU"
    CLUSTER_EXPANSION = "CLUSTER EXPANSION"
    DIAGNOSTICS = "DIAGNOSTICS"
    NDU = "NDU"
    ADMIN = "ADMIN"


class ProcedureName(ConstantClass):
    REPLACE_DAE_CONTROLLER = "Replace DAE Controller"
    REPLACE_STORAGE_CONTROLLER_PSU = "Replace Storage Controller PSU"
    REPLACE_STORAGE_CONTROLLER = "Replace Storage Controller"
    REPLACE_SSD = "Replace SSD"
    REPLACE_INFINIBAND_SWITCH = "Replace Infiniband Switch"
    REPLACE_DAE_ROW_CONTROLLER = "Replace DAE ROW Controller"
    REPLACE_DAE_FAN = "Replace DAE FAN"
    REPLACE_DAE_PSU = "Replace DAE PSU"
    REPLACE_DAE = "Replace DAE"
    REPLACE_STORAGE_CONTROLLER_HDD = "Replace Storage Controller HDD"
    CLUSTER_SCALE_UP = "Cluster Scale Up"
    ID_LED_TEST = "ID LED Test"
    SYSTEM_HEALTH_CHECK = "System Health Check"
    HW_BIST = "HW BIST"
    COLLECT_LOG_BUNDLE = "Collect Log Bundle"


class ProcedurePageInformation(ConstantClass):
    PSNT = "PSNT"
    SSD = "SSD"
    SERIAL_NUMBER = "S/N"
    VERIFICATION_CODE = "Verification code"
    UNKNOWN = "Unknown"
    SERIAL_NUMBER_FULL = "Serial number"
