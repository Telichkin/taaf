from TAAF.support import ConstantClass


class ConnectivitySettingsField(ConstantClass):
    IP_ADDRESS = "IP ADDRESS"
    XMS_TECH_PASSWORD = "XMS TECH PASSWORD"
    SSH_ROOT_PASSWORD = "SSH ROOT PASSWORD"
    IPMI_ADMIN_PASSWORD = "IPMI ADMIN PASSWORD"


class DefaultConnectivitySettings(ConstantClass):
    IP_ADDRESS = "vxms-xbrickdrm1241.xiodrm.lab.emc.com"
