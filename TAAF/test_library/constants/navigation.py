from TAAF.support import ConstantClass


class SiteNavigation(ConstantClass):
    HARDWARE = "Hardware page"
    INVENTORY = "Inventory page"
    ALERTS = "Alerts page"
    LOG = "Log page"
