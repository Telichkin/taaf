from TAAF.support import ConstantClass


class By(ConstantClass):
    ID = "ID"
    XPATH = "XPATH"
    LINK_TEXT = "LINK TEXT"
    PARTIAL_LINK_TEXT = "PARTIAL LINK TEXT"
    NAME = "NAME"
    TAG_NAME = "TAG NAME"
    CLASS_NAME = "CLASS NAME"
    CSS_SELECTOR = "CSS SELECTOR"
