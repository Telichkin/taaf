from TAAF.support import ConstantClass


class BrowserBrand(ConstantClass):
    CHROME = "CHROME"
    FIREFOX = "FIREFOX"
    IE = "IE"
    SAFARI = "SAFARI"


class BrowserType(ConstantClass):
    STANDALONE = "STANDALONE"
    REMOTE = "REMOTE"
