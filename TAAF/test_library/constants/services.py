from TAAF.support import ConstantClass


class Service(ConstantClass):
    JYTHON = "JYTHON"
    MONGODB = "MONGODB"
    METEOR = "METEOR"
    REPORT_VIEWER = "REPORT_VIEWER"


class LogType(ConstantClass):
    STDOUT = "STDOUT"
    STDERR = "STDERR"
