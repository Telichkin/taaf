from TAAF.support import ConstantClass


class LoginPageMessages(ConstantClass):
    NO_SERVER_CONNECTIVITY = "NO SERVER CONNECTIVITY"
    NO_LDAP_SERVER = "LDAP server unavailable"
    INVALID_PASSWORD = "invalid password"
    INVALID_LOGIN = "User not found."
