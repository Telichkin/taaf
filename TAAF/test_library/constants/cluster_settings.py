from TAAF.support import ConstantClass


class ElementNumberInOneCluster(ConstantClass):
    CLUSTER = 1
    BRICKS = 2
    STORAGE_CONTROLLERS = 2
    INFINIBAND_SWITCHES = 2
    DAE = 1
    SSD = 18
    DAE_PSU = 2
    DAE_CONTROLLERS = 2
    DAE_ROW_CONTROLLERS = 8
    SC_PSU = 4
    LOCAL_DISKS = 6
    DPG = 1


class ClusterElementName(ConstantClass):
    CLUSTER = "Cluster"
    BRICKS = "Bricks"
    INFINIBAND_SWITCHES = "InfiniBand Switches"
    STORAGE_CONTROLLERS = "Storage Controllers"
    DAE = "DAE"
    SSD = "SSD"
    DAE_PSU = "DAE-PSU"
    DAE_CONTROLLERS = "DAE Controllers"
    DAE_ROW_CONTROLLERS = "DAE Row Controllers"
    SC_PSU = "SC-PSU"
    LOCAL_DISKS = "Local Disks"
    DPG = "DPG"
    DAE_FAN = "Fan"


class OneBrickInventory(ConstantClass):
    SELF = (
        (ClusterElementName.STORAGE_CONTROLLERS, ElementNumberInOneCluster.STORAGE_CONTROLLERS),
        (ClusterElementName.DAE, ElementNumberInOneCluster.DAE),
        (ClusterElementName.SSD, ElementNumberInOneCluster.SSD),
        (ClusterElementName.DAE_PSU, ElementNumberInOneCluster.DAE_PSU),
        (ClusterElementName.DAE_CONTROLLERS, ElementNumberInOneCluster.DAE_CONTROLLERS),
        (ClusterElementName.DAE_ROW_CONTROLLERS, ElementNumberInOneCluster.DAE_ROW_CONTROLLERS),
        (ClusterElementName.SC_PSU, ElementNumberInOneCluster.SC_PSU),
        (ClusterElementName.LOCAL_DISKS, ElementNumberInOneCluster.LOCAL_DISKS),
        (ClusterElementName.DPG, ElementNumberInOneCluster.DPG),
    )
