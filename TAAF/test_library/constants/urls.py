from settings import FRONTEND_URI
from TAAF.support import ConstantClass


class Url(ConstantClass):
    LOGIN_PAGE = f"{FRONTEND_URI}/login"
    HARDWARE_PAGE = f"{FRONTEND_URI}/hardware"
    INVENTORY_PAGE = f"{FRONTEND_URI}/inventory"
    LOG_PAGE = f"{FRONTEND_URI}/log"
    ALERTS_PAGE = f"{FRONTEND_URI}/alerts"
    PROCEDURE_PAGE = f"{FRONTEND_URI}/procedures"

