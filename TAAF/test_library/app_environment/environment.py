from TAAF.settings import ServiceLogPath
from TAAF.test_library.constants import Service, LogType


class Environment:
    @staticmethod
    def get_log(service, log_type):
        path_by_service_and_log_type = {
            (Service.JYTHON, LogType.STDOUT): ServiceLogPath.JYTHON_STDOUT,
            (Service.JYTHON, LogType.STDERR): ServiceLogPath.JYTHON_STDERR,
            (Service.METEOR, LogType.STDOUT): ServiceLogPath.METEOR_STDOUT,
            (Service.METEOR, LogType.STDERR): ServiceLogPath.METEOR_STDERR,
            (Service.MONGODB, LogType.STDOUT): ServiceLogPath.MONGODB_STDOUT,
            (Service.MONGODB, LogType.STDERR): ServiceLogPath.MONGODB_STDERR,
            (Service.REPORT_VIEWER, LogType.STDOUT): ServiceLogPath.REPORT_VIEWER_STDOUT,
            (Service.REPORT_VIEWER, LogType.STDERR): ServiceLogPath.REPORT_VIEWER_STDERR,
        }

        with open(path_by_service_and_log_type[(service, log_type)], errors='replace') as log:
            return log.read()
