import shutil


class EnvironmentChecker:
    MONGO = "mongod"
    JAVA = "Java"
    JYTHON = "jython"
    ANT = "ant"
    NODE = "node"
    NPM = "npm"
    METEOR = "meteor"

    def __init__(self):
        self._error_messages = []

    @property
    def error_message(self):
        return '\n'.join(self._error_messages)

    def is_environment_prepared(self):
        self._find_errors_in_executable(EnvironmentChecker.MONGO)
        self._find_errors_in_executable(EnvironmentChecker.JAVA)
        self._find_errors_in_executable(EnvironmentChecker.JYTHON)
        self._find_errors_in_executable(EnvironmentChecker.ANT)
        self._find_errors_in_executable(EnvironmentChecker.NODE)
        self._find_errors_in_executable(EnvironmentChecker.NPM)
        self._find_errors_in_executable(EnvironmentChecker.METEOR)
        return not self._error_messages

    def _find_errors_in_executable(self, executable):
        if not EnvironmentChecker._is_executable_in_path(executable):
            self._error_messages.append(f"{executable} missing in PATH. Add path to {executable} in PATH and try again")

    @staticmethod
    def _is_executable_in_path(executable):
        path = shutil.which(executable)
        return path is not None


