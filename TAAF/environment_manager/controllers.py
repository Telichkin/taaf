import os
import subprocess

from .checkers import EnvironmentChecker
from .services import JythonServer, MongoDB, MeteorApp
from TAAF.settings import BackendSettings, FrontendSettings


class ApplicationController:
    @staticmethod
    def start(path_to_project):
        ApplicationController._check_environment()
        print("--- Starting application ---")
        try:
            BackendController.start(path_to_project)
            FrontendController.start(path_to_project)
        except Exception as e:
            print(e)
            ApplicationController.stop()
            raise EnvironmentError("Application not started. See logs for details")

        print("--- Application started ---")

    @staticmethod
    def _check_environment():
        checker = EnvironmentChecker()
        if not checker.is_environment_prepared():
            raise EnvironmentError(checker.error_message)

    @staticmethod
    def stop():
        print("--- Stopping Application ---")
        FrontendController.stop()
        BackendController.stop()
        print("--- Application Stopped ---")


class BackendController:
    @staticmethod
    def start(path_to_project):
        BackendController._compile_java(path_to_project)
        MongoDB.start(path_to_project, timeout=15)
        JythonServer.start(path_to_project, timeout=60)

    @staticmethod
    def _compile_java(path_to_project):
        build_file = os.path.normpath(os.path.join(path_to_project, BackendSettings.ANT_BUILD_FILE))
        subprocess.run([EnvironmentChecker.ANT, "-f", build_file, "jar"], shell=True, check=True,
                       stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print("Java files compiled")

    @staticmethod
    def stop():
        JythonServer.stop()
        MongoDB.stop()


class FrontendController:
    @staticmethod
    def start(path_to_project):
        FrontendController._install_npm_packages(path_to_project)
        MeteorApp.start(path_to_project, timeout=180)

    @staticmethod
    def _install_npm_packages(path_to_project):
        path_to_meteor_app = os.path.normpath(os.path.join(path_to_project, FrontendSettings.SOURCES))
        os.chdir(path_to_meteor_app)
        subprocess.run(["npm", "install"], shell=True, check=True,
                       stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print("NPM packages installed")

    @staticmethod
    def stop():
        MeteorApp.stop()
