import os
import time
import psutil
import shutil
from abc import ABCMeta, abstractstaticmethod
import subprocess

from ..checkers import EnvironmentChecker
from TAAF.support import rerun_on_exception
from TAAF.settings import BackendSettings, FrontendSettings, ReporterPath, ReporterURI, ServiceLogPath, \
    ServicePidFilePath


def create_logs_and_pids_directories():
    if not os.path.exists(ServicePidFilePath.ROOT):
        os.makedirs(ServicePidFilePath.ROOT)

    if not os.path.exists(ServiceLogPath.ROOT):
        os.makedirs(ServiceLogPath.ROOT)

create_logs_and_pids_directories()


class Service(metaclass=ABCMeta):
    _STDOUT_LOG_PATH = None
    _STDERR_LOG_PATH = None
    _PID_FILE_PATH = None
    _START_PATTERN = None
    _STDIN_FILE_PATH = os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "logs/all_in"))

    @classmethod
    def start(cls, path_to_project, timeout):
        if cls._is_started():
            print(f"{cls.__name__} already started")
        else:
            cls._actions_before_start(path_to_project)
            pid = cls._run_subprocess_and_get_pid(path_to_project)
            cls._save_process_pid(pid)
            if cls._START_PATTERN:
                cls._wait_service_start(timeout)
            cls._post_started_verification()
            print(f"{cls.__name__} started")

    @classmethod
    def _is_started(cls):
        if cls._is_pid_file_not_exist_or_empty():
            return False
        else:
            process_still_running = psutil.pid_exists(cls._get_pid_from_pid_file())
            if process_still_running:
                return True
            else:
                cls._clear_pid_file()
                return False

    @classmethod
    def _actions_before_start(cls, path_to_project):
        pass

    @classmethod
    def _is_pid_file_not_exist_or_empty(cls):
        if not os.path.exists(cls._PID_FILE_PATH):
            return True
        else:
            return os.path.getsize(cls._PID_FILE_PATH) == 0

    @classmethod
    def _get_pid_from_pid_file(cls):
        with open(cls._PID_FILE_PATH, "r") as pid_file:
            return int(pid_file.readline())

    @classmethod
    def _clear_pid_file(cls):
        with open(cls._PID_FILE_PATH, "w+"):
            pass

    @classmethod
    def _run_subprocess_and_get_pid(cls, path_to_project):
        with open(cls._STDOUT_LOG_PATH, "w+") as stdout_file, \
                open(cls._STDERR_LOG_PATH, "w+") as stderr_file, \
                open(cls._STDIN_FILE_PATH, "w+") as stdin_file:
            run_subprocess = cls._subprocess_factory_method(path_to_project, stdout_file,
                                                            stderr_file, stdin_file)
        return run_subprocess.pid

    @staticmethod
    @abstractstaticmethod
    def _subprocess_factory_method(path_to_project, stdout_file, stderr_file, stdin_file) -> subprocess.Popen:
        pass

    @classmethod
    def _save_process_pid(cls, pid):
        with open(cls._PID_FILE_PATH, "w+") as pid_file:
            pid_file.write(str(pid))

    @classmethod
    def _wait_service_start(cls, timeout):
        start_time = time.time()
        with open(cls._STDOUT_LOG_PATH, "r") as stdout:
            stdout.seek(0, 2)
            while True:
                line = stdout.readline()
                if line and cls._START_PATTERN in line:
                    break
                if time.time() - start_time > timeout:
                    raise TimeoutError(f"{cls.__name__} not started correctly after {timeout} seconds")

    @classmethod
    def _post_started_verification(cls):
        for _ in range(5):
            if not cls._is_started():
                raise EnvironmentError(f"{cls.__name__} crashed. Check logs")
            time.sleep(1)

    @classmethod
    def stop(cls):
        if cls._is_started():
            cls._kill_process_tree()
            cls._clear_pid_file()
            print(f"{cls.__name__} stopped")
        else:
            print(f"{cls.__name__} is not running")

    @classmethod
    def _kill_process_tree(cls):
        pid = cls._get_pid_from_pid_file()
        process = psutil.Process(pid)
        for child_process in process.children(recursive=True):
            cls._kill_process(child_process)
        cls._kill_process(process)

    @classmethod
    @rerun_on_exception(max_rerun_number=5)
    def _kill_process(cls, process):
        try:
            process.terminate()
        except psutil.NoSuchProcess:
            pass


class JythonServer(Service):
    _STDOUT_LOG_PATH = ServiceLogPath.JYTHON_STDOUT
    _STDERR_LOG_PATH = ServiceLogPath.JYTHON_STDERR
    _PID_FILE_PATH = ServicePidFilePath.JYTHON
    _START_PATTERN = "Server started"

    @classmethod
    def _actions_before_start(cls, path_to_project):
        JythonServer._replace_procedure_files(path_to_project)

    @staticmethod
    def _replace_procedure_files(path_to_project):
        for procedure_file_name in os.listdir(os.path.join(path_to_project, BackendSettings.FAKE_PROCEDURES_PATH)):
            full_fake_path = os.path.join(path_to_project, BackendSettings.FAKE_PROCEDURES_PATH, procedure_file_name)
            full_real_path = os.path.join(path_to_project, BackendSettings.REAL_PROCEDURES_PATH, procedure_file_name)
            shutil.copyfile(full_fake_path, full_real_path)

    @staticmethod
    def _subprocess_factory_method(path_to_project, stdout_file, stderr_file, stdin_file):
        cwd = os.path.normpath(os.path.join(path_to_project, BackendSettings.SOURCES))
        classpath = JythonServer._get_classpath(path_to_project)
        pythonpath = JythonServer._get_pythonpath(path_to_project)
        jython_server = subprocess.Popen(
            [EnvironmentChecker.JYTHON, "-J-cp", f"{classpath}",
             f"-Dpython.path={pythonpath}", "serverstart.py", "-c",
             os.path.normpath(os.path.join(path_to_project, BackendSettings.CI_CONFIG_PATH))],
            cwd=cwd, stdout=stdout_file, stderr=stderr_file, stdin=stdin_file,
            creationflags=subprocess.CREATE_NEW_PROCESS_GROUP
        )
        return jython_server

    @staticmethod
    def _get_classpath(path_to_project):
        full_classpath = [os.path.normpath(os.path.join(path_to_project, classpath_item))
                          for classpath_item in BackendSettings.CLASSPATH]
        return ";".join(full_classpath)

    @staticmethod
    def _get_pythonpath(path_to_project):
        full_pythonpath = [os.path.normpath(os.path.join(path_to_project, pythonpath_item))
                           for pythonpath_item in BackendSettings.PYTHONPATH]
        return ";".join(full_pythonpath)


class MongoDB(Service):
    _STDOUT_LOG_PATH = ServiceLogPath.MONGODB_STDOUT
    _STDERR_LOG_PATH = ServiceLogPath.MONGODB_STDERR
    _PID_FILE_PATH = ServicePidFilePath.MONGODB
    _START_PATTERN = "waiting for connections on port"

    @staticmethod
    def _subprocess_factory_method(path_to_project, stdout_file, stderr_file, stdin_file):
        mongodb = subprocess.Popen([EnvironmentChecker.MONGO], stdout=stdout_file,
                                   stderr=stderr_file, stdin=stdin_file,
                                   creationflags=subprocess.CREATE_NEW_PROCESS_GROUP)
        return mongodb


class MeteorApp(Service):
    _STDOUT_LOG_PATH = ServiceLogPath.METEOR_STDOUT
    _STDERR_LOG_PATH = ServiceLogPath.METEOR_STDERR
    _PID_FILE_PATH = ServicePidFilePath.METEOR
    _START_PATTERN = "App running at"

    @staticmethod
    def _subprocess_factory_method(path_to_project, stdout_file, stderr_file, stdin_file):
        cwd = os.path.normpath(os.path.join(path_to_project, FrontendSettings.SOURCES))
        meteor = subprocess.Popen(["meteor", "run"], cwd=cwd, stdout=stdout_file,
                                  stderr=stderr_file, stdin=stdin_file, shell=True,
                                  creationflags=subprocess.CREATE_NEW_PROCESS_GROUP)
        return meteor


class ReportViewer(Service):
    _STDOUT_LOG_PATH = ServiceLogPath.REPORT_VIEWER_STDOUT
    _STDERR_LOG_PATH = ServiceLogPath.REPORT_VIEWER_STDERR
    _PID_FILE_PATH = ServicePidFilePath.REPORT_VIEWER
    _START_PATTERN = None

    @staticmethod
    def _subprocess_factory_method(path_to_project, stdout_file, stderr_file, stdin_file):
        report_viewer = subprocess.Popen(["python", "-m", "http.server", ReporterURI.LOCAL_REPORT_PORT],
                                         cwd=ReporterPath.RENDERED_REPORT_DIRECTORY,
                                         stderr=stderr_file, stdout=stdout_file, stdin=stdin_file,
                                         creationflags=subprocess.CREATE_NEW_PROCESS_GROUP)
        return report_viewer
