"""Tech Advisor Automation Framework (TAAF)

Usage:
    TAAF env up <project-path>
    TAAF env down
    TAAF test
    TAAF report save
    TAAF report open

"""
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from docopt import docopt

from .cli import cli_processing


if __name__ == '__main__':
    arguments = docopt(__doc__, version='TAAF 0.1')
    cli_processing(arguments)
