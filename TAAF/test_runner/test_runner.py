import os

import pytest

from TAAF.settings import ReporterPath


def run_tests(cmd_args: list = None):
    project_dir_path = os.path.normpath(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
    test_directory = os.path.normpath(os.path.join(project_dir_path, "tests"))
    additional_args = cmd_args or []
    pytest.main(["--alluredir", f"{ReporterPath.RAW_REPORT_DIRECTORY}",
                 "--junitxml", f"{ReporterPath.JUNIT_REPORT_DIRECTORY}",
                 *additional_args, test_directory])
