from os import path

from .support import ConstantClass


class ApplicationPath(ConstantClass):
    __TAAF = path.dirname(path.abspath(__file__))
    CLI = path.join(__TAAF, "cli")
    REPORTER = path.join(__TAAF, "reporter")
    NOTIFIER = path.join(__TAAF, "notifier")
    ENVIRONMENT_MANAGER = path.join(__TAAF, "environment_manager")
    TEST_RUNNER = path.join(__TAAF, "test_runner")


class ReporterPath(ConstantClass):
    RAW_REPORT_DIRECTORY = path.join(ApplicationPath.REPORTER, "raw_report")
    JUNIT_REPORT_DIRECTORY = path.join(ApplicationPath.REPORTER, "junit_report/report.xml")
    RENDERED_REPORT_DIRECTORY = path.join(ApplicationPath.REPORTER, "rendered_report")
    ALLURE_BAT = path.join(ApplicationPath.REPORTER, "allure", "bin", "allure.bat")
    REPORT_LINK = path.join(ApplicationPath.REPORTER, "report_link.log")


class NotifierPath(ConstantClass):
    MARKDOWN_TEMPLATE = path.join(ApplicationPath.NOTIFIER, "templates", "markdown.md")


class ReporterURI(ConstantClass):
    LOCAL_REPORT_PORT = "5000"
    LOCAL_REPORT_SERVER = f"http://localhost:{LOCAL_REPORT_PORT}"
    UPLOAD_REPORT = f"http://10.76.236.147/reports/upload/zip"


class ServiceLogPath(ConstantClass):
    ROOT = path.join(ApplicationPath.ENVIRONMENT_MANAGER, "services", "logs")
    JYTHON_STDOUT = path.join(ROOT, "jython_stdout.log")
    JYTHON_STDERR = path.join(ROOT, "jython_stderr.log")
    METEOR_STDOUT = path.join(ROOT, "meteor_stdout.log")
    METEOR_STDERR = path.join(ROOT, "meteor_stderr.log")
    MONGODB_STDOUT = path.join(ROOT, "mongodb_stdout.log")
    MONGODB_STDERR = path.join(ROOT, "mongodb_stderr.log")
    REPORT_VIEWER_STDOUT = path.join(ROOT, "report_viewer_stdout.log")
    REPORT_VIEWER_STDERR = path.join(ROOT, "report_viewer_stderr.log")
    ALL_STDIN = path.join(ROOT, "all_in")


class ServicePidFilePath(ConstantClass):
    ROOT = path.join(ApplicationPath.ENVIRONMENT_MANAGER, "services", "pids")
    JYTHON = path.join(ROOT, "jython_server")
    METEOR = path.join(ROOT, "meteor")
    MONGODB = path.join(ROOT, "mongodb")
    REPORT_VIEWER = path.join(ROOT, "report_viewer")


class BackendSettings(ConstantClass):
    CLASSPATH = [
        path.normpath(classpath) for classpath in [
            "dist/bin/tang_utils.jar",
            "emc.xtreamio.tang.java/lib/*",
            "emc.xtreamio.ta.py/src/conf/ci",  # FIXME: need to discuss about writing all logs directly into stdout
        ]
    ]
    PYTHONPATH = [
        path.normpath(pythonpath) for pythonpath in [
            "emc.xtreamio.ta.py",
            "emc.xtreamio.ta.py/src",
            "emc.xtreamio.ta.py/src/data",
            "emc.xtreamio.ta.py/src/data_model",
            "emc.xtreamio.ta.py/src/procedures",
            "emc.xtreamio.ta.py/src/brokers",
            "emc.xtreamio.ta.py/src/rest",
            "emc.xtreamio.ta.py/src/scripts",
            "emc.xtreamio.ta.py/src/infra",
            "emc.xtreamio.ta.py/3party/webpy-master",
            "emc.xtreamio.ta.py/src/webserverpy",
            "emc.xtreamio.ta.py/src/xio",
            "emc.xtreamio.ta.py/3party/jsonpickle-0.9.3",
        ]
    ]
    ANT_BUILD_FILE = "build.xml"
    SOURCES = path.join("emc.xtreamio.ta.py", "src")
    CI_CONFIG_PATH = path.join(SOURCES, "conf", "ci", "sys.ini")
    FAKE_PROCEDURES_PATH = path.join(SOURCES, "procedures", "ci_desc")
    REAL_PROCEDURES_PATH = path.join(SOURCES, "procedures", "desc")


class FrontendSettings:
    SOURCES = "emc.xtreamio.tang.app"
