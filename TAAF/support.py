import time


class Wait:
    def __init__(self, timeout, exception=TimeoutError()):
        self._timeout = timeout
        self._exception = exception

    def until(self, condition_function, function_args: tuple=()):
        start_time = time.time()
        while not condition_function(*function_args):
            if (time.time() - start_time) > self._timeout:
                raise self._exception
            time.sleep(0.01)

    def until_not(self, condition_function, function_args: tuple=()):
        start_time = time.time()
        while condition_function(*function_args):
            if (time.time() - start_time) > self._timeout:
                raise self._exception
            time.sleep(0.01)


class ConstantMetaclass(type):
    def __setattr__(cls, key, value):
        if key in cls.__dict__:
            raise AttributeError("Read-only class")
        else:
            super().__setattr__(key, value)

    def __delattr__(cls, key):
        raise AttributeError("Read-only class")


class ConstantClass(metaclass=ConstantMetaclass):
        def __setattr__(self, key, value):
            raise AttributeError("Read-only class")

        def __getattr__(self, item):
            raise AttributeError("Read-only class")

        @classmethod
        def items(cls):
            constants = [item for item in cls.__dict__.items() if cls.__is_valid_constant(item)]
            return constants

        @classmethod
        def constant_names(cls):
            constant_names = [item[0] for item in cls.__dict__.items() if cls.__is_valid_constant(item)]
            return constant_names

        @classmethod
        def constant_values(cls):
            constant_values = [item[1] for item in cls.__dict__.items() if cls.__is_valid_constant(item)]
            return constant_values

        @classmethod
        def __is_valid_constant(cls, constant):
            return not constant[0].startswith("__") and \
                   constant[0].isupper() and \
                   constant[1] is not None


def rerun_on_exception(max_rerun_number, exception=Exception):
    def rerun_decorator(fn):
        def wrapper(*args, **kwargs):
            for rerun_number in range(max_rerun_number):
                try:
                    return fn(*args, **kwargs)
                except exception as e:
                    if rerun_number == max_rerun_number:
                        raise e
                    else:
                        time.sleep(1)
        return wrapper
    return rerun_decorator
