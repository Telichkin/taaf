import os
import shutil
import subprocess
import webbrowser

import requests

from TAAF.environment_manager import ReportViewer
from TAAF.settings import ReporterPath, ReporterURI, ApplicationPath


class Report:
    @staticmethod
    def save(name=None, local=False):
        Report.__render_allure_report()
        if local:
            Report.__save_local_report()
        else:
            Report.__save_remote_report(name)
        print("Report saved. Use 'TAAF report open' for open report")

    @staticmethod
    def __render_allure_report():
        subprocess.run([ReporterPath.ALLURE_BAT, "generate", "-o", ReporterPath.RENDERED_REPORT_DIRECTORY,
                        ReporterPath.RAW_REPORT_DIRECTORY], shell=True, check=True, stdout=subprocess.PIPE)

    @staticmethod
    def __save_local_report():
        with open(ReporterPath.REPORT_LINK, "w+") as report_link:
            report_link.write(ReporterURI.LOCAL_REPORT_SERVER)

    @staticmethod
    def __save_remote_report(name):
        if name is None:
            name = Report.__generate_name()
        path_to_zip = Report.__generate_zip_and_get_path(name)
        try:
            with open(path_to_zip, "rb") as zip_file:
                response = requests.post(ReporterURI.UPLOAD_REPORT, files={"file": zip_file}, timeout=40)
        finally:
            os.remove(path_to_zip)
        Report.__save_link_from_response(response)

    @staticmethod
    def __generate_name():
        return "Test Report"

    @staticmethod
    def __generate_zip_and_get_path(name):
        os.chdir(ApplicationPath.REPORTER)
        shutil.make_archive(name, "zip", ReporterPath.RENDERED_REPORT_DIRECTORY)
        return os.path.join(ApplicationPath.REPORTER, f"{name}.zip")

    @staticmethod
    def __save_link_from_response(response):
        link = response.json()['uri']
        with open(ReporterPath.REPORT_LINK, "w+") as report_link:
            report_link.write(link)

    @staticmethod
    def open():
        report_link = Report.__get_saved_link()
        if not report_link:
            FileExistsError("Report is not saved. Nothing to show")
        elif report_link == ReporterURI.LOCAL_REPORT_SERVER:
            webbrowser.open_new(report_link)
            ReportViewer.start(None, timeout=3)
        else:
            webbrowser.open_new(report_link)

    @staticmethod
    def __get_saved_link():
        with open(ReporterPath.REPORT_LINK) as report_link:
            return report_link.read().strip(' ')
