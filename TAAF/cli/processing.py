from TAAF.reporter import Report
from TAAF.test_runner import run_tests
from TAAF.environment_manager import ApplicationController


ENV_COMMAND = 'env'
START_COMMAND = 'up'
PROJECT_PATH = '<project-path>'
STOP_COMMAND = 'down'
REPORT_COMMAND = 'report'
REPORT_OPEN = 'open'
REPORT_SAVE = 'save'
TEST = 'test'


def cli_processing(arguments):
    if arguments[ENV_COMMAND]:
        if arguments[START_COMMAND]:
            ApplicationController.start(arguments[PROJECT_PATH])
        elif arguments[STOP_COMMAND]:
            ApplicationController.stop()
    elif arguments[REPORT_COMMAND]:
        if arguments[REPORT_OPEN]:
            Report.open()
        elif arguments[REPORT_SAVE]:
            Report.save(local=True)
    elif arguments[TEST]:
        run_tests()
